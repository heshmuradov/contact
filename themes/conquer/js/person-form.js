jQuery(document).ready(function($) {
	$('#Person_date_of_birth').datepicker({format: 'yyyy-mm-dd'});

	 $(window).load(function() {
	        // this code will run after all other $(document).ready() scripts
	        // have completely finished, AND all page elements are fully loaded.
		 handleCompanyFormJSvalidate( $('#company_type').val() );	
	});

	loadCompanyFormFragment( $('#company_type').val() );
	$('#company_type_section .btn').bind('click', function(){
		$('#company_type').val($(this).val());		
		handleCompanyFormJSvalidate( $(this).val() );	
		loadCompanyFormFragment($(this).val());
	});
	
	//when the 'company_type' value is not new, we should not validate the CompanyForm
	function handleCompanyFormJSvalidate( company_type ){
		if (company_type  == 'new' )
	    {
	        enableFieldsValidation($('#person-form'), 'Company', 'name');
	        enableFieldsValidation($('#person-form'), 'Company', 'association_id');
	        enableFieldsValidation($('#person-form'), 'Address', 'Company_0_street');
	        enableFieldsValidation($('#person-form'), 'Address', 'Company_0_zip');
	        enableFieldsValidation($('#person-form'), 'Address', 'Company_0_city');
	        enableFieldsValidation($('#person-form'), 'Address', 'Company_0_country');   
	    }
	    else
	    {
	    	disableFieldsValidation($('#person-form'), 'Company', 'name');
	        disableFieldsValidation($('#person-form'), 'Company', 'association_id');
	        disableFieldsValidation($('#person-form'), 'Address', 'Company_0_street');
	        disableFieldsValidation($('#person-form'), 'Address', 'Company_0_zip');
	        disableFieldsValidation($('#person-form'), 'Address', 'Company_0_city');
	        disableFieldsValidation($('#person-form'), 'Address', 'Company_0_country');
	    }
	}
	
	function loadCompanyFormFragment(company_type)
	{
		if(company_type == 'exist'){
			hideCompanyForm();
			addCompanyField();
		}else if(company_type == 'new'){
			hideCompanyForm();
			addNewCompanyForm();
		}else if(company_type == 'no'){
			hideCompanyForm();
		}
	}

	function hideCompanyForm()
	{
		$('#company > div').hide();
	}

	function addCompanyField()
	{
		$('#company #exist-company').show();
	}

	function addNewCompanyForm()
	{
		$('#company #new-company').show();
	}
	
	function enableFieldsValidation(form, model, fieldName) {
		
	    // Restore validation for model attributes
	    $.each(form.data('settings').attributes, function (i, attribute) {

	        if (attribute.model == model && attribute.id == (model + '_' + fieldName))
	        {
	            if (attribute.hasOwnProperty('disabledClientValidation')) {

	                // Restore validation function
	                attribute.clientValidation = attribute.disabledClientValidation;
	                delete attribute.disabledClientValidation;

	                // Restore sucess css class
	                attribute.successCssClass = attribute.disabledSuccessCssClass;
	                delete attribute.disabledSuccessCssClass;
	            }
	        }
	    });
	}

	function disableFieldsValidation(form, model, fieldName) {

	    $.each(form.data('settings').attributes, function (i, attribute) {

	        if (attribute.model == model && attribute.id == (model + '_' + fieldName))
	        {
	            if (!attribute.hasOwnProperty('disabledClientValidation')) {

	                // Remove validation function
	                attribute.disabledClientValidation = attribute.clientValidation;
	                delete attribute.clientValidation;

	                // Reset style of elements
	                $.fn.yiiactiveform.getInputContainer(attribute, form).removeClass(
	                    attribute.validatingCssClass + ' ' +
	                    attribute.errorCssClass + ' ' +
	                    attribute.successCssClass
	                );

	                // Reset validation status
	                attribute.status = 2;

	                // Hide error messages
	                form.find('#' + attribute.errorID).toggle(false);

	                // Dont make it 'green' when validation is called
	                attribute.disabledSuccessCssClass = attribute.successCssClass;
	                attribute.successCssClass = '';
	            }
	        }
	    });
	}
});