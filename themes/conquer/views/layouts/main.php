<?php Yii::app()->clientScript->registerCoreScript('jquery');?>
<?php Yii::app()->clientScript->registerCoreScript('jquery.migrate');?>
<?php Yii::app()->clientScript->registerCoreScript('jquery.ui');?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
  <meta charset="utf-8" />
  <title>Conquer | Page Layouts - Sidebar Fixed Page</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="width=device-width, initial-scale=1.0" name="viewport" />
  <meta content="" name="description" />
  <meta content="" name="author" />
  <meta name="MobileOptimized" content="320">
  <!-- BEGIN GLOBAL MANDATORY STYLES -->
  <link href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
  <link href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
  <link href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
  <!-- END GLOBAL MANDATORY STYLES -->
  <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/plugins/gritter/css/jquery.gritter.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/plugins/select2/select2_conquer.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/plugins/clockface/css/clockface.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/plugins/bootstrap-datepicker/css/datepicker.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/plugins/bootstrap-timepicker/compiled/timepicker.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/plugins/bootstrap-colorpicker/css/colorpicker.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/plugins/jquery-multi-select/css/multi-select.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-conquer.css"/>
  <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/plugins/jquery-tags-input/jquery.tagsinput.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css">

  <!-- BEGIN THEME STYLES -->
  <link href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/css/style-conquer.css" rel="stylesheet" type="text/css"/>
  <link href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/css/style.css" rel="stylesheet" type="text/css"/>
  <link href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
  <link href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/css/plugins.css" rel="stylesheet" type="text/css"/>

  <!--<link href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/plugins/bootstrap-datepicker/css/datepicker.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/css/plugins.css" rel="stylesheet" type="text/css" />-->

  <link href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
  <link href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/css/custom.css" rel="stylesheet" type="text/css"/>
  <link href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/css/main.css" rel="stylesheet" />
  <!-- END THEME STYLES -->
  <link rel="shortcut icon" href="favicon.ico" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed page-sidebar-fixed">
  <!-- BEGIN HEADER -->
  <div class="header navbar navbar-inverse navbar-fixed-top">
    <!-- BEGIN TOP NAVIGATION BAR -->
    <div class="header-inner">
      <!-- BEGIN LOGO -->
      <a class="navbar-brand" href="index.html">
      <img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/img/logo.png" alt="logo" class="img-responsive" />
      </a>
      <form class="search-form search-form-header" role="form" action="index.html" >
        <div class="input-icon right">
          <i class="icon-search"></i>
          <input type="text" class="form-control input-medium input-sm" name="query" placeholder="Search...">
        </div>
      </form>
      <!-- END LOGO -->
      <!-- BEGIN RESPONSIVE MENU TOGGLER -->
      <a href="javascript:;" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
      <img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/img/menu-toggler.png" alt="" />
      </a>
      <!-- END RESPONSIVE MENU TOGGLER -->
      <!-- BEGIN TOP NAVIGATION MENU -->
      <ul class="nav navbar-nav pull-right">
        <!-- BEGIN NOTIFICATION DROPDOWN -->
        <li class="dropdown" id="header_notification_bar">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
            data-close-others="true">
          <i class="icon-warning-sign"></i>
          <span class="badge badge-success">6</span>
          </a>
          <ul class="dropdown-menu extended notification">
            <li>
              <p>You have 14 new notifications</p>
            </li>
            <li>
              <ul class="dropdown-menu-list scroller" style="height: 250px;">
                <li>
                  <a href="#">
                  <span class="label label-sm label-icon label-success"><i class="icon-plus"></i></span>
                  New user registered.
                  <span class="time">Just now</span>
                  </a>
                </li>
                <li>
                  <a href="#">
                  <span class="label label-sm label-icon label-danger"><i class="icon-bolt"></i></span>
                  Server #12 overloaded.
                  <span class="time">15 mins</span>
                  </a>
                </li>
                <li>
                  <a href="#">
                  <span class="label label-sm label-icon label-warning"><i class="icon-bell"></i></span>
                  Server #2 not responding.
                  <span class="time">22 mins</span>
                  </a>
                </li>
                <li>
                  <a href="#">
                  <span class="label label-sm label-icon label-info"><i class="icon-bullhorn"></i></span>
                  Application error.
                  <span class="time">40 mins</span>
                  </a>
                </li>
                <li>
                  <a href="#">
                  <span class="label label-sm label-icon label-danger"><i class="icon-bolt"></i></span>
                  Database overloaded 68%.
                  <span class="time">2 hrs</span>
                  </a>
                </li>
                <li>
                  <a href="#">
                  <span class="label label-sm label-icon label-danger"><i class="icon-bolt"></i></span>
                  2 user IP blocked.
                  <span class="time">5 hrs</span>
                  </a>
                </li>
                <li>
                  <a href="#">
                  <span class="label label-sm label-icon label-warning"><i class="icon-bell"></i></span>
                  Storage Server #4 not responding.
                  <span class="time">45 mins</span>
                  </a>
                </li>
                <li>
                  <a href="#">
                  <span class="label label-sm label-icon label-info"><i class="icon-bullhorn"></i></span>
                  System Error.
                  <span class="time">55 mins</span>
                  </a>
                </li>
                <li>
                  <a href="#">
                  <span class="label label-sm label-icon label-danger"><i class="icon-bolt"></i></span>
                  Database overloaded 68%.
                  <span class="time">2 hrs</span>
                  </a>
                </li>
              </ul>
            </li>
            <li class="external">
              <a href="#">See all notifications <i class="icon-angle-right"></i></a>
            </li>
          </ul>
        </li>
        <!-- END NOTIFICATION DROPDOWN -->
        <!-- BEGIN INBOX DROPDOWN -->
        <li class="dropdown" id="header_inbox_bar">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
            data-close-others="true">
          <i class="icon-envelope"></i>
          <span class="badge badge-info">5</span>
          </a>
          <ul class="dropdown-menu extended inbox">
            <li>
              <p>You have 12 new messages</p>
            </li>
            <li>
              <ul class="dropdown-menu-list scroller" style="height: 250px;">
                <li>
                  <a href="inbox.html?a=view">
                  <span class="photo"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/img/avatar2.jpg" alt=""/></span>
                  <span class="subject">
                  <span class="from">Lisa Wong</span>
                  <span class="time">Just Now</span>
                  </span>
                  <span class="message">
                  Vivamus sed auctor nibh congue nibh. auctor nibh
                  auctor nibh...
                  </span>
                  </a>
                </li>
                <li>
                  <a href="inbox.html?a=view">
                  <span class="photo"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/img/avatar3.jpg" alt=""/></span>
                  <span class="subject">
                  <span class="from">Richard Doe</span>
                  <span class="time">16 mins</span>
                  </span>
                  <span class="message">
                  Vivamus sed congue nibh auctor nibh congue nibh. auctor nibh
                  auctor nibh...
                  </span>
                  </a>
                </li>
                <li>
                  <a href="inbox.html?a=view">
                  <span class="photo"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/img/avatar1.jpg" alt=""/></span>
                  <span class="subject">
                  <span class="from">Bob Nilson</span>
                  <span class="time">2 hrs</span>
                  </span>
                  <span class="message">
                  Vivamus sed nibh auctor nibh congue nibh. auctor nibh
                  auctor nibh...
                  </span>
                  </a>
                </li>
                <li>
                  <a href="inbox.html?a=view">
                  <span class="photo"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/img/avatar2.jpg" alt=""/></span>
                  <span class="subject">
                  <span class="from">Lisa Wong</span>
                  <span class="time">40 mins</span>
                  </span>
                  <span class="message">
                  Vivamus sed auctor 40% nibh congue nibh...
                  </span>
                  </a>
                </li>
                <li>
                  <a href="inbox.html?a=view">
                  <span class="photo"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/img/avatar3.jpg" alt=""/></span>
                  <span class="subject">
                  <span class="from">Richard Doe</span>
                  <span class="time">46 mins</span>
                  </span>
                  <span class="message">
                  Vivamus sed congue nibh auctor nibh congue nibh. auctor nibh
                  auctor nibh...
                  </span>
                  </a>
                </li>
              </ul>
            </li>
            <li class="external">
              <a href="inbox.html">See all messages <i class="icon-angle-right"></i></a>
            </li>
          </ul>
        </li>
        <!-- END INBOX DROPDOWN -->
        <!-- BEGIN TODO DROPDOWN -->
        <li class="dropdown" id="header_task_bar">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
          <i class="icon-ok"></i>
          <span class="badge badge-warning">5</span>
          </a>
          <ul class="dropdown-menu extended tasks">
            <li>
              <p>You have 12 pending tasks</p>
            </li>
            <li>
              <ul class="dropdown-menu-list scroller" style="height: 250px;">
                <li>
                  <a href="#">
                  <span class="task">
                  <span class="desc">New release v1.2</span>
                  <span class="percent">30%</span>
                  </span>
                  <span class="progress">
                  <span style="width: 40%;" class="progress-bar progress-bar-success" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100">
                  <span class="sr-only">40% Complete</span>
                  </span>
                  </span>
                  </a>
                </li>
                <li>
                  <a href="#">
                  <span class="task">
                  <span class="desc">Application deployment</span>
                  <span class="percent">65%</span>
                  </span>
                  <span class="progress progress-striped">
                  <span style="width: 65%;" class="progress-bar progress-bar-danger" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100">
                  <span class="sr-only">65% Complete</span>
                  </span>
                  </span>
                  </a>
                </li>
                <li>
                  <a href="#">
                  <span class="task">
                  <span class="desc">Mobile app release</span>
                  <span class="percent">98%</span>
                  </span>
                  <span class="progress">
                  <span style="width: 98%;" class="progress-bar progress-bar-success" aria-valuenow="98" aria-valuemin="0" aria-valuemax="100">
                  <span class="sr-only">98% Complete</span>
                  </span>
                  </span>
                  </a>
                </li>
                <li>
                  <a href="#">
                  <span class="task">
                  <span class="desc">Database migration</span>
                  <span class="percent">10%</span>
                  </span>
                  <span class="progress progress-striped">
                  <span style="width: 10%;" class="progress-bar progress-bar-warning" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100">
                  <span class="sr-only">10% Complete</span>
                  </span>
                  </span>
                  </a>
                </li>
                <li>
                  <a href="#">
                  <span class="task">
                  <span class="desc">Web server upgrade</span>
                  <span class="percent">58%</span>
                  </span>
                  <span class="progress progress-striped">
                  <span style="width: 58%;" class="progress-bar progress-bar-info" aria-valuenow="58" aria-valuemin="0" aria-valuemax="100">
                  <span class="sr-only">58% Complete</span>
                  </span>
                  </span>
                  </a>
                </li>
                <li>
                  <a href="#">
                  <span class="task">
                  <span class="desc">Mobile development</span>
                  <span class="percent">85%</span>
                  </span>
                  <span class="progress progress-striped">
                  <span style="width: 85%;" class="progress-bar progress-bar-success" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100">
                  <span class="sr-only">85% Complete</span>
                  </span>
                  </span>
                  </a>
                </li>
                <li>
                  <a href="#">
                  <span class="task">
                  <span class="desc">New UI release</span>
                  <span class="percent">18%</span>
                  </span>
                  <span class="progress progress-striped">
                  <span style="width: 18%;" class="progress-bar progress-bar-important" aria-valuenow="18" aria-valuemin="0" aria-valuemax="100">
                  <span class="sr-only">18% Complete</span>
                  </span>
                  </span>
                  </a>
                </li>
              </ul>
            </li>
            <li class="external">
              <a href="#">See all tasks <i class="icon-angle-right"></i></a>
            </li>
          </ul>
        </li>
        <!-- END TODO DROPDOWN -->
        <li class="devider">&nbsp;</li>
        <!-- BEGIN USER LOGIN DROPDOWN -->
        <li class="dropdown user">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
          <img alt="" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/img/avatar3_small.jpg"/>
          <span class="username">Nick</span>
          <i class="icon-angle-down"></i>
          </a>

<?php $this->widget('zii.widgets.CMenu',array(
        'encodeLabel'=>false,
        'items'=>array(
          array('label'=>'<i class="icon-user"></i> '.Yii::app()->user->name,
           'url'=>array('/profile/profile/update', 'id'=>Yii::app()->user->id),
           'visible'=>!Yii::app()->user->isGuest,
          ),

          array('label'=>'<i class="icon-calendar"></i> My Calendar',
           'url'=>array('#', 'id'=>Yii::app()->user->id),
           'visible'=>!Yii::app()->user->isGuest,
          ),

          array('label'=>'<i class="icon-envelope"></i> My Inbox  <span class="badge badge-danger">3</span>',
           'url'=>array('#', 'id'=>Yii::app()->user->id),
           'visible'=>!Yii::app()->user->isGuest,
          ),

          array('label'=>'<i class="icon-tasks"></i> My Tasks  <span class="badge badge-success">2</span>',
           'url'=>array('#', 'id'=>Yii::app()->user->id),
           'visible'=>!Yii::app()->user->isGuest,
          ),


          array('label'=>'',
           'itemOptions'=>array('class'=>'divider'),
          ),
          array('label'=>'<i class="icon-key"></i> Log Out',
            'url'=>array('/site/logout'),
            'visible'=>!Yii::app()->user->isGuest,
          ),
        ),
        'htmlOptions' => array('class'=>'dropdown-menu'),
       )); ?>


        </li>
        <!-- END USER LOGIN DROPDOWN -->
      </ul>
      <!-- END TOP NAVIGATION MENU -->
    </div>
    <!-- END TOP NAVIGATION BAR -->
  </div>
  <!-- END HEADER -->
  <div class="clearfix"></div>
  <!-- BEGIN CONTAINER -->
  <div class="page-container">
    <!-- BEGIN SIDEBAR -->
    <div class="page-sidebar navbar-collapse collapse">
      <!-- BEGIN SIDEBAR MENU -->
        <?php $this->widget('zii.widgets.CMenu',array(
          'htmlOptions' => array(
            'class' => 'page-sidebar-menu',
            // 'style' => 'overflow: hidden; width: auto; height: 263px;',
          ),
          'items'=>array(
            array('label' => '<div class="sidebar-toggler"></div><div class="clearfix"></div>'),

           array('label'=> '<i class="icon-home"></i><span class="title">Home</span>', 'url'=>array('/site/index')),
           array('label'=>'Login', 'url'=>array('/user/user/login'), 'visible'=>Yii::app()->user->isGuest),

           array('label'=>'<i class="icon-user"></i><span class="title">User Management</span>', 'url'=>array('/user/user/admin'), 'visible'=>Yii::app()->user->isAdmin()),

           array('label'=>'<i class="icon-book"></i><span class="title">Contact</span><span class="arrow"></span>', 'url'=>'javascript:;', 'visible'=>!Yii::app()->user->isGuest,
           'itemOptions' => array('class'=>'has-sub'),
            'items'=>array(
              array('label'=>'Company', 'url'=>array('/contact/company/index'), 'visible'=>!Yii::app()->user->isGuest),
              array('label'=>'Personal', 'url'=>array('/contact/person/index'), 'visible'=>!Yii::app()->user->isGuest),
           )),
          ),
          'submenuHtmlOptions' => array('class'=>"sub-menu"),
          'activateParents'=>true,
          'encodeLabel'=>false
          )); ?>
      <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->
    <div class="page-content">
      <!-- BEGIN PAGE HEADER-->
      <div class="row">
        <div class="col-md-12">
          <!-- BEGIN PAGE TITLE & BREADCRUMB-->
          <h3 class="page-title">
            Sidebar Fixed Page <small>sidebar fixed page</small>
          </h3>
    <?php if(isset($this->breadcrumbs)):?>
      <?php $this->widget('BBreadcrumbs', array(
       'links'=>$this->breadcrumbs,
      )); ?><!-- breadcrumbs -->
    <?php endif?>

      <?php /* ?>
      <ul class="breadcrumb">
       <li>
       <i class="icon-home"></i>
       <a href="#">Home</a> <span class="divider">/</span>
       </li>
       <li><a href="#">Page Title</a></li>
      </ul><?php */ ?>
          <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
      </div>
      <!-- END PAGE HEADER-->
      <!-- BEGIN PAGE CONTENT-->
      <div class="row">
        <div class="col-md-12">
           <?php echo $content; ?>
        </div>
      </div>
      <!-- END PAGE CONTENT-->
    </div>
    <!-- END PAGE -->
  </div>
  <!-- END CONTAINER -->
  <!-- BEGIN FOOTER -->
  <div class="footer">
    <div class="footer-inner">
      2013 &copy; Conquer by keenthemes.
    </div>
    <div class="footer-tools">
      <span class="go-top">
      <i class="icon-angle-up"></i>
      </span>
    </div>
  </div>
  <!-- END FOOTER -->
  <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
  <!-- BEGIN CORE PLUGINS -->
  <!--[if lt IE 9]>
  <script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/plugins/respond.min.js"></script>
  <script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/plugins/excanvas.min.js"></script>
  <![endif]-->
  <!--<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>-->
  <!--<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>-->
  <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
  <!--<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>-->
  <script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
  <script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript" ></script>
  <script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
  <script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>
  <script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
  <script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript" ></script>
  <!-- END CORE PLUGINS -->

  <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/plugins/fuelux/js/spinner.min.js"></script>
   <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/plugins/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js"></script>
   <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/plugins/select2/select2.min.js"></script>
   <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
   <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
   <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/plugins/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/plugins/bootstrap-daterangepicker/moment.min.js"></script>
   <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
   <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
   <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>
   <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/plugins/jquery.input-ip-address-control-1.0.min.js"></script>
   <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
   <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/plugins/jquery-multi-select/js/jquery.quicksearch.js"></script>
   <script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/plugins/jquery.pwstrength.bootstrap/src/pwstrength.js" type="text/javascript" ></script>
   <script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js" type="text/javascript" ></script>
   <script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/plugins/jquery-tags-input/jquery.tagsinput.min.js" type="text/javascript" ></script>
   <script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/plugins/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript" ></script>
   <script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/plugins/bootstrap-markdown/lib/markdown.js" type="text/javascript" ></script>
   <script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript" ></script>



  <!--<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
  <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/plugins/clockface/js/clockface.js"></script>
  <script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js" type="text/javascript" ></script>
  <script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/plugins/bootstrap-daterangepicker/moment.min.js" type="text/javascript"></script>
  <script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>-->
  <script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/scripts/app.js"></script>
  <script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/scripts/form-components.js"></script>
  <script>
    jQuery(document).ready(function() {
      App.init();
      FormComponents.init();
    });
  </script>
  <!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>