<?php 
$this->pageTitle = Yum::t('Password recovery');

$this->breadcrumbs=array(
	Yum::t('Login') => Yum::module()->loginUrl,
	Yum::t('Restore'));

?>
<?php if(Yum::hasFlash()) {
echo '<div class="success">';
echo Yum::getFlash(); 
echo '</div>';
} else {
//echo '<h2>'.Yum::t('Password recovery').'</h2>';
?>

<div id="logo" class="center"></div>

<div id="login">
<?php echo CHtml::beginForm(array('', 'post', array('id'=>'forgotform', 'class'=>"form-vertical no-padding no-margin hide"))); ?>

	<?php echo CHtml::errorSummary($form); ?>
  
  <p class="center">Enter your e-mail address below to reset your password.</p>
  
  <div class="control-group">
    <div class="controls">
      <div class="input-prepend">
        <span class="add-on"><i class="icon-envelope"></i></span>
        <?php echo CHtml::activeTextField($form,'login_or_email',array('id'=>"input-email",'placeholder'=>"Email")) ?>
      </div>
    </div>
    <div class="space10"></div>
  </div>
  
  <?php echo CHtml::submitButton(Yum::t('Restore'), array('id'=>"forget-btn", 'class'=>"btn btn-block btn-inverse")); ?>

<?php echo CHtml::endForm(); ?>
</div><!-- form -->
<?php } ?>
