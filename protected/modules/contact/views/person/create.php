<?php
/* @var $this PersonController */
/* @var $model Person */

$this->breadcrumbs=array(
	'People'=>array('index'),
	'Create',
);

?>

<?php $this->renderPartial('../company/_admin_header'); ?>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>