<?php $monitoringTypes = MonitoringType::model()->findAll(); ?>
<?php if(!empty($monitoringTypes)):?>
	<h3 class="form-section">Monitoring</h3>
	<div class="row">
	<?php foreach ($monitoringTypes as $monitoringType):?>
			<div class="col-md-6">
				<div class="col-md-9 col-md-offset-3">
				<?php $monitoringPerson = $model->getMonitoringPerson($monitoringType->id);?>
				<?php echo CHtml::hiddenField('Monitoring['. $monitoringType->id .'][person_id]', $monitoringPerson->id)?>
				<div class="input-icon">
				<i class="icon-search"></i>
				<?php
				$this->widget('application.components.widgets.BJuiAutoComplete', array(
				'name'=>'Monitoring['. $monitoringType->id .'][person]',
				'source'=>$this->createUrl('/contact/person/getPersonList'),
				'value' => $monitoringPerson->getFullName(),
				// additional options for the autocomplete plugin
				'options'=>array(
					'minLength'=>'1',
					'select' => 'js:function(event, ui){ jQuery("#"+$(this).attr("id")+"_id").val(ui.item["id"]); }'
				 ),
				'htmlOptions'=>array(
					'placeholder' => $monitoringType->name,
					'class' => 'form-control',
				),

				));
				?>
				<span class="help-block">Enter the first letter to find <?php echo strtolower($monitoringType->name);?><span>
				</div>
			<?php //echo CHtml::error("[{$monitoringType->id}]person_id"); ?>
				</div>
			</div>
	<?php endforeach;?>
	</div>
<?php endif;?>