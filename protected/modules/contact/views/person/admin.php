<?php
/* @var $this PersonController */
/* @var $model Person */

$this->breadcrumbs=array(
	'People'=>array('index'),
	'Manage',
);

Yii::app()->clientScript->registerCss('personPhoto', "
.personPhoto{
 	display: table-cell;
    height: 80px;
    text-align: center;
    vertical-align: middle;
    width: 80px;
	border: 2px solid #DDDDDD;
    box-shadow: 0 1px 3px rgba(0, 0, 0, 0.055);
    line-height: 20px;
    padding: 4px;
    transition: all 0.2s ease-in-out 0s;
}
.personPhoto i{
	font-size: 50px;
    line-height: normal;
}
.dropdown-menu li{
	margin-bottom: 3px;
}		
.Customer{
	border-left: medium solid Green;
}
.Provider{
	border-left: medium solid Blue;
}
.Partner{
	border-left: medium solid Red;
}
.Prospect{
	border-left: medium solid Orange;
}
.person_type_list{
	float: left;
    height: 80px;
    width: 5px;
}		
	
");
Yii::app()->clientScript->registerScript('person-group-filter', "

	$('.person-group-filter a').live('click', function(){
		$('#Person_person_groups').val( $(this).attr('data-id') );
		$('#person-grid').yiiGridView('update', {
			data: $('#yw0').serialize() 
		});
		return false;
	});
");
?>

<?php $form=$this->beginWidget('CActiveForm', array(
		'action'=>Yii::app()->createUrl($this->route),
		'method'=>'get',
		)); ?>
	<?php echo $form->hiddenField($model,'person_groups'); ?>
<?php $this->endWidget(); ?>

<?php $this->renderPartial('../company/_admin_header'); ?>

<?php 
$template = '<div class="portlet">
					<div class="portlet-title">
						<div class="caption">
							<i class="icon-bookmark"></i>Person List
						</div>
						<div class="tools">
							{summary}
						</div>
					</div>
					<div class="portlet-body">
							<div class="btn-group btn-group-solid">
								<button data-toggle="dropdown" class="btn btn-info dropdown-toggle" type="button">
								<i class="icon-sitemap"></i> Group <i class="icon-angle-down"></i>
								</button>
								<ul class="dropdown-menu person-group-filter">
								';
$personType = CHtml::listData( PersonType::model()->search()->getData(), 'id', 'name' );
foreach ( $personType as $id=>$name){
	$template .= '<li>
					<a class="'.$name.'" href="#" data-id="'.$id.'">'.$name.'</a>
				  </li>';
}
$template .= '
								</ul>
							</div>
							{items}
							{pager}
					</div>
					</div>
			   </div>';
?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'person-grid',
	'dataProvider'=>isset($operator) ? $model->search( $operator, $company_name ) : $model->search(),
	'itemsCssClass'=>'table table-striped table-hover',
	'template' => $template,
	'ajaxUrl'=> Yii::app()->request->getUrl(),
	'pager'=>array(
			'class'=>'CLinkPager',
			'header'=>false,
			//'firstPageCssClass'=>'hidden',
			//'lastPageCssClass'=>'hidden',
			'htmlOptions'=>array('class'=>'pager'),
			'prevPageLabel'=>'← Previous',
			'previousPageCssClass'=>'',
			'nextPageLabel'=>'Next →',
			'nextPageCssClass'=>'',
			'selectedPageCssClass'=>'active',
			'hiddenPageCssClass'=>'disabled',
	),
   'pagerCssClass'=>'dataTables_paginate paging_bootstrap',
	'columns'=>array(
		array('name'=>'person_groups', 'header'=>'#', 'type'=>'html', 'value'=>function($data){
			$html = '';
			foreach ($data->types as $person_type) {
				$html .= '<div class="person_type_list '.$person_type->name.'"></div>';
			}
			return $data->getPersonTypes();
		}),
		array('name'=>'photo', 'header'=>'Avatar', 'type'=>'html', 
			  'value'=>'!empty($data->photo) ? CHtml::image(Yii::app()->request->baseUrl . str_replace(".", "-80x80.", $data->photo),"", array("width"=>"80px", "height"=>"80px")):
						\'<div class="personPhoto"><i class="icon-upload-alt"></i></div>\' ;'),
		array('name'=>'first_name' ),
		array('name'=>'last_name' ),
		array('name'=>'phone', 'header'=>'Phone' ),
		array('name'=>'mobile_phone'),
		array('name'=>'email' ),
		array('name'=>'person_companies', 'header'=>'Company' , 'value'=>function($data) {
						                           $company_name = array();
						                           foreach ($data->companies as $company) {
						                              $company_name[] = $company->name;
						                           }
						                           return implode(', ', $company_name);
					                         	},
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{view}',
			'viewButtonOptions' => array('class'=>'btn btn-default'),
			'viewButtonImageUrl'=>false,
			'viewButtonLabel'=>'View <i class="icon-eye-open"></i>',
		),
	),
)); ?>