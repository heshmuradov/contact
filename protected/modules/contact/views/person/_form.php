<?php
/* @var $this PersonController */
/* @var $model Person */
/* @var $form CActiveForm */
Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/js/person-form.js');
?>
<div class="portlet">
	<div class="portlet-title">
		<div class="caption">
			<i class="icon-reorder"></i>
			<?php echo $model->isNewRecord ? 'Create Personal Contact' : 'Update Personal Contact ' . $model->id;?>
		</div>
		<!--div class="tools">
			<a class="collapse" href="javascript:;"></a>
			<a class="config" data-toggle="modal" href="#portlet-config"></a>
			<a class="reload" href="javascript:;"></a>
			<a class="remove" href="javascript:;"></a>
		</div-->
	</div>
	<div class="portlet-body form">
		<?php $form=$this->beginWidget('CActiveForm', array(
				'id'=>'person-form',
				'enableAjaxValidation'=>false,
				'htmlOptions' => array('class' => 'form-horizontal', 'enctype' => 'multipart/form-data'),
				'enableClientValidation'=>true,
				'clientOptions'=>array(
					'validateOnSubmit'=>true,
					'inputContainer' =>'div.form-group',
					'errorCssClass'=>'has-error',
				)
)); ?>
		<div class="form-body">
			<?php $this->renderPartial('_person_company_form', array('model' => $model, 'form' => $form)); ?>
			
			<h3 class="form-section">Person Info</h3>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group col-md-9 col-md-offset-3">
					<?php echo $form->textField($model,'first_name', array('placeholder'=>"First Name",'class'=>"form-control")); ?>
					<?php echo $form->error($model,'first_name'); ?>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group col-md-9 col-md-offset-3">
					<?php echo $form->textField($model,'last_name', array('placeholder'=>"Last Name",'class'=>"form-control")); ?>
					<?php echo $form->error($model,'last_name'); ?>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
					<div class="form-group col-md-9 col-md-offset-3">
					<label class="control-label" style="float: left;">Gender</label>
					<div class="radio-list col-md-9">
					<?php echo $form->radioButtonList($model,'gender', Yii::app()->params['gender'], 
							array('separator'=>'', 
								  'template'=>'<label class="radio-inline control-label">{input}{label}</label>',
							)); ?>										
					</div>
					<?php echo $form->error($model,'gender', array('style'=>'clear:both;')); ?>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group col-md-9 col-md-offset-3">	
					<?php echo $form->textField($model,'date_of_birth',array('placeholder'=>"Date Of Birth yyyy-mm-dd",'class'=>"form-control") ); ?>
					<?php echo $form->error($model,'date_of_birth'); ?>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-6">
					<div class="form-group col-md-9 col-md-offset-3">
						<div class="input-icon">
							<i class="icon-envelope"></i>
							<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>255,'placeholder'=>"Email",'class'=>"form-control")); ?>
							<?php echo $form->error($model,'email'); ?>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group col-md-9 col-md-offset-3">
					<div class="input-icon">
						<i class="icon-phone"></i>
						<?php echo $form->textField($model,'fax',array('size'=>25,'maxlength'=>25,'placeholder'=>"Fax",'class'=>"form-control")); ?>
						<?php echo $form->error($model,'fax'); ?>
					</div>
					</div>
				</div>
			</div>
			
			<div class="row">		
				<div class="col-md-6">
					<div class="form-group col-md-9 col-md-offset-3">
					<div class="input-icon">
						<i class="icon-phone"></i>
						<?php echo $form->textField($model,'phone',array('size'=>25,'maxlength'=>25,'placeholder'=>"Phone",'class'=>"form-control")); ?>
						<?php echo $form->error($model,'phone'); ?>
					</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group col-md-9 col-md-offset-3">
					<div class="input-icon">
						<i class="icon-phone"></i>
						<?php echo $form->textField($model,'mobile_phone',array('placeholder'=>"Mobile Phone",'class'=>"form-control")); ?>
						<?php echo $form->error($model,'mobile_phone'); ?>
					</div>
					</div>
				</div>
			</div>

			<?php $this->renderPartial('../address/_address_block', array('form' => $form, 'model' => $model)); ?>

			<h3 class="form-section">Avatar</h3>
			<div class="row">
				<div class="col-md-6">
				<div class="col-md-9 col-md-offset-3">
				<?php 
				$img = ( $model->isNewRecord || empty($model->photo) ) ? '': CHtml::image(Yii::app()->request->baseUrl . $model->photo,"image",array("max-width"=>200));
				$class = ($img == '') ? 'fileupload-new': 'fileupload-exists';			
				?>
					<div class="fileupload <?php echo $class?>" data-provides="fileupload">
						<div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
							<img src="<?php echo Yii::app()->theme->baseUrl?>/images/AAAAAA&amp;text=no+image.gif" alt="">
						</div>
						<div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 10px;">
							<?php echo $img;?>
						</div>
						<div>
							<span class="btn btn-default btn-file">
								<span class="fileupload-new"><i class="icon-paper-clip"></i> Select image</span>
								<span class="fileupload-exists"><i class="icon-undo"></i> Change</span>
								<?php echo $form->fileField($model,'photo'); ?>
							</span>							
							<a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="icon-trash"></i> Remove</a>
							<?php echo $form->error($model,'photo'); ?>
						</div>
					</div>
				</div>
				</div>
			</div>
			<?php $this->renderPartial('_monitoring', array('form' => $form, 'model' => $model))?>
			
			<h3 class="form-section">Type</h3>
			<div class="row">
				<div class="col-md-6">
				<div class="col-md-9 col-md-offset-3 checkbox-list">
				<?php echo CHtml::checkBoxList('type', 
						CHtml::listData($model->types, 'id', 'id'), 
						CHtml::listData(PersonType::model()->findAll(), 'id', 'name'),
						array( 'separator'=>'',
							   'labelOptions'=>array('style'=>'display:inline;'),
							   'template'=>'<label class="checkbox-inline">{input}{label}</label>',
							 )
						); ?>
				</div>
				</div>
			</div>
		</div>
		<div class="form-actions fluid">
			<div class="row">
				<div class="col-md-6">
					<div class="col-md-offset-3 col-md-9">
						<button  type="submit" class="btn btn-info">
							<i class="icon-ok"></i>
							<?php echo $model->isNewRecord ? 'Create' : 'Save';?>
						</button>
					</div>
				</div>
				<div class="col-md-6"></div>
			</div>
		</div>
		<?php $this->endWidget(); ?>
	</div>
	<!-- form -->
</div>
