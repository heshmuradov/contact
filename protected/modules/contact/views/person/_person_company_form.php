<h3 class="form-section">Related Company</h3>
<div id="company_type_section" class="row" style="text-align: center;">
	<?php
	$company_type = Yii::app()->request->getParam('company_type');
	if(empty($company_type)){
		$company_type = !empty($model->companies) ? 'exist' : 'no';
	}
	?>
	<div class="btn-group btn-group-solid">
		<button value="no" class="btn btn-info" type="button">No Company</button>
		<button value="exist" class="btn btn-warning" type="button">Existing Company</button>
		<button value="new" class="btn btn-success" type="button">New Company</button>
	</div>
	<?php 
	echo CHtml::hiddenField('company_type', $company_type);	
	?>
</div>

<?php 
$error = '';
$error_message = '';
if( Yii::app()->user->hasFlash('company_error') ){
	$error = 'has-error';
	$error_message = Yii::app()->user->getFlash('company_error');
}

?>
<div id="company">
	<div id="exist-company">
		<h3 class="form-section">Company Info</h3>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group col-md-9 col-md-offset-3 <?php echo $error;?>">
		<?php if (!empty($model->companies)): ?>
			<?php foreach ($model->companies as $item => $company):?>				
					<?php echo CHtml::hiddenField('ExistCompany['.$item.'][id]', $company->id); ?>
					<div class="input-icon">
						<i class="icon-search"></i>
						<?php
							$this->widget('application.components.widgets.BJuiAutoComplete', array(
							'name'=>'ExistCompany['.$item.'][name]',
							'source'=>$this->createUrl('/contact/company/getCompanyList'),
							'value' => $company->name,
							// additional options for the autocomplete plugin
							'options'=>array(
								'minLength'=>'1',
								'select' => 'js:function(event, ui){ jQuery("#"+$(this).attr("id").replace("name", "id")).val(ui.item["id"]); }'
							 ),
							'htmlOptions'=>array(
								'style'=>'width: 200px;',
								'placeholder' => 'Company name',
								'class' => 'form-control',
							),
						));
	
						?>
						<span class="help-block">Enter the first letter to find a company<span>
					</div>
			<?php endforeach;?>
		<?php else:?>
				<?php echo CHtml::hiddenField('ExistCompany[id]', Yii::app()->request->getParam('company_id')); ?>
				<div class="input-icon">
					<i class="icon-search"></i>
					<?php
						$this->widget('application.components.widgets.BJuiAutoComplete', array(
						'name'=>'ExistCompany[name]',
						'source'=>$this->createUrl('/contact/company/getCompanyList'),
						// additional options for the autocomplete plugin
						'options'=>array(
							'minLength'=>'1',
							'select' => 'js:function(event, ui){ jQuery("#"+$(this).attr("id").replace("name", "id")).val(ui.item["id"]); }'
						 ),
						'htmlOptions'=>array(
							'style'=>'width: 200px;',
							'placeholder' => 'Company name',
							'class' => 'form-control',
						),
					));
					?>
					<span class="help-block">Enter the first letter to find a company<span>
				</div>
		<?php endif;?>
					<div class="errorMessage"><?php echo $error_message;?></div>
				</div>
			</div>
		</div>
	</div>
	
	<div id="new-company">
		<?php $this->renderPartial('../company/_partial_form', array('model' => new Company, 'form' => $form)); ?>
	</div>
</div>