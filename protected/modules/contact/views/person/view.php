<?php
/* @var $this PersonController */
/* @var $model Person */

$this->breadcrumbs=array(
	'People'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Person', 'url'=>array('index')),
	array('label'=>'Create Person', 'url'=>array('create')),
	array('label'=>'Update Person', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Person', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Person', 'url'=>array('admin')),
);
?>

<?php $this->renderPartial('../company/_admin_header'); ?>
<h1>View Person #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'first_name',
		'last_name',
		'gender' => array(
			'label' => 'Gender',
			'type' => 'raw',
			'value' => Yii::app()->params['gender'][CHtml::encode($model->gender)]
		),
		'date_of_birth',
		'phone',
		'mobile_phone',
		'fax',
		'email',
		array(
			'label' => 'Photo',
			'type' => 'raw',
			'value' => CHtml::image(Yii::app()->request->baseUrl . $model->photo),
		),
		array(
			'label' => 'Type',
			'type' => 'raw',
			'value' => implode(', ', CHtml::listData($model->types, 'id', 'name')),
		),
		'company' => array(
			'label' => 'Company',
			'type' => 'raw',
			'value' => !empty($model->companies) ? implode(', ', CHtml::listData($model->companies, 'id', 'name')) : '',
		),
	),
)); ?>
