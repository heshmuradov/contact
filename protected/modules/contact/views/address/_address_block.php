<?php
$i = !is_array($model->addresses) ? 0 : count($model->addresses) - 1;
if( $i < 0){
	$i = 0;
}
$model_name = get_class($model);
Yii::app()->clientScript->registerScript('address-form-'.$model_name, "$(function(){
	$('a[href=\"#tab_address_${model_name}_0\"]').tab('show');
	
	$('#Address_${model_name}_0_zip').select2({
		query: function (query) {
			var data = {results: []};
			if(query.term) {
				data.results.push({id: query.term, text: query.term});
			}
			
			$.ajax({
				url: 'index.php?r=contact/company/getZipList',
				data: {term: query.term},
				dataType: 'json',
				async: false,
				success: $.proxy(function(json) {
					$.each(json, function(index, object){
						data.results.push({id: object.zip, text: object.label, cityName: object.cityName, countryCode: object.countryCode});
					});
				}, data)
			});
			
			query.callback(data);
		},
		initSelection : function (element, callback) {
	        var data = {id: element.val(), text: element.val()};
	        callback(data);
	    },
		formatResult: zipFormatResult,
		formatSelection: zipFormatSelection,
		escapeMarkup: function(m) { return m; }
	});
	
	$('#Address_${model_name}_0_city').select2({
		query: function (query) {
			var data = {results: []};
			if(query.term) {
				data.results.push({id: query.term, text: query.term});
			}
			
			$.ajax({
				url: 'index.php?r=contact/company/getCityList',
				data: {term: query.term},
				dataType: 'json',
				async: false,
				success: $.proxy(function(json) {
					$.each(json, function(index, object){
						data.results.push({id: object.cityName, text: object.label, zip: object.zip, countryCode: object.countryCode});
					});
				}, data)
			});
			
			query.callback(data);
		},
		initSelection : function (element, callback) {
	        var data = {id: element.val(), text: element.val()};
	        callback(data);
	    },
		formatResult: cityFormatResult,
		formatSelection: cityFormatSelection,
		escapeMarkup: function(m) { return m; }
	});
	
	$('#Address_${model_name}_0_country').select2({
		 formatResult: format,
		 formatSelection: format,
		 escapeMarkup: function(m) { return m; }
	});
	
	$(document).on('select2-selecting', '.select2-zip', function(e){
		$(this).closest('.tab-pane').find('.select2-city').select2('val', e.object.cityName);
		$(this).closest('.tab-pane').find('.select2-country').select2('val', e.object.countryCode);
	});
	
	$(document).on('select2-selecting', '.select2-city', function(e){
		$(this).closest('.tab-pane').find('.select2-zip').select2('val', e.object.zip);
		$(this).closest('.tab-pane').find('.select2-country').select2('val', e.object.countryCode);
	});
})

function zipFormatResult(zip) {
	return zip.text;
}

function zipFormatSelection(zip) {
	return zip.id;
}

function cityFormatResult(city) {
	return city.text;
}

function cityFormatSelection(city) {
	return city.id;
}

function format(state) {
	if (!state.id) return state.text; // optgroup
	return \"<img class='flag' src='".Yii::app()->theme->baseUrl."/assets/img/flags/\" + state.id.toLowerCase() + \".png'/>\" + state.text;
}

function TabNavigation(order) {

	this.order = order;
	
	this.title = function getTitle() {
		var title;
		var order = this.order;
		
		switch(order) {
		case 1:
			title = 'First Address';break;
		case 2:
			title = 'Second Address';break;
		case 3:
			title = 'Third Address';break;
		default:
			title = order + 'th Address';break;
		}
		
		return title;
	}
}

$(document).on('click', '#${model_name}_tabs .icon-power-off', function(){
	if(confirm('Are you sure to remove this address?')) {
		var tab_id = $(this).data('tab-id');
		$('#'+tab_id).remove();
		$(this).closest('li').remove();
		$('#${model_name}_tabs ul.nav-tabs li').each(function( index ) {
			var tab_id = $(this).find('.icon-power-off').data('tab-id');

			if(tab_id) {
				var tabNav = new TabNavigation(index+1);
				var html = '<a href=\"#'+tab_id+'\" data-toggle=\"tab\">'
				         + '<i class=\"icon-reorder\"></i> '+tabNav.title()+' <i class=\"icon-power-off\" data-tab-id=\"'+tab_id+'\"></i>'
				         + '</a>';
				$(this).html(html);
			}
		});
		$('a[href=\"#tab_address_${model_name}_0\"]').tab('show');
	}
});

var ${model_name}_i = $i;

$('a[href=\"#tab_new_address_${model_name}\"]').on('shown.bs.tab', function (e) {
	${model_name}_i++;
	var tab_id = 'tab_address_${model_name}_' + ${model_name}_i;

	var count = $(this).closest('ul').children().length;
	var tabNav = new TabNavigation(count);
	var html = '<li class=\"\">'
	         + '<a href=\"#'+tab_id+'\" data-toggle=\"tab\">'
	         + '<i class=\"icon-reorder\"></i> '+tabNav.title()+' <i class=\"icon-power-off\" data-tab-id=\"'+tab_id+'\"></i>'
	         + '</a></li>';

	$(this).parent().before(html);

	var html = '<div id=\"'+tab_id+'\" class=\"tab-pane\">'
			 + $(\"#tab_new_address_${model_name}\").html().replace(/Address\[${model_name}\]\[/g,'Address[${model_name}]['+${model_name}_i).replace(/Address_${model_name}_/g,'Address_${model_name}_'+${model_name}_i+'_')
			 + '</div>';
	$(\"#tab_new_address_${model_name}\").before(html);

	$('a[href=\"#'+tab_id+'\"]').tab('show');
	
	$('#Address_${model_name}_'+${model_name}_i+'_zip').select2({
		query: function (query) {
			var data = {results: []};
			if(query.term) {
				data.results.push({id: query.term, text: query.term});
			}
			
			$.ajax({
				url: 'index.php?r=contact/company/getZipList',
				data: {term: query.term},
				dataType: 'json',
				async: false,
				success: $.proxy(function(json) {
					$.each(json, function(index, object){
						data.results.push({id: object.zip, text: object.label, cityName: object.cityName, countryCode: object.countryCode});
					});
				}, data)
			});
			
			query.callback(data);
		},
		initSelection : function (element, callback) {
	        var data = {id: element.val(), text: element.val()};
	        callback(data);
	    },
		formatResult: zipFormatResult,
		formatSelection: zipFormatSelection,
		escapeMarkup: function(m) { return m; }
	});
	
	$('#Address_${model_name}_'+${model_name}_i+'_city').select2({
		query: function (query) {
			var data = {results: []};
			if(query.term) {
				data.results.push({id: query.term, text: query.term});
			}
			
			$.ajax({
				url: 'index.php?r=contact/company/getCityList',
				data: {term: query.term},
				dataType: 'json',
				async: false,
				success: $.proxy(function(json) {
					$.each(json, function(index, object){
						data.results.push({id: object.cityName, text: object.label, zip: object.zip, countryCode: object.countryCode});
					});
				}, data)
			});
			
			query.callback(data);
		},
		initSelection : function (element, callback) {
	        var data = {id: element.val(), text: element.val()};
	        callback(data);
	    },
		formatResult: cityFormatResult,
		formatSelection: cityFormatSelection,
		escapeMarkup: function(m) { return m; }
	});
	
	$('#Address_${model_name}_'+${model_name}_i+'_country').select2({
		 formatResult: format,
		 formatSelection: format,
		 escapeMarkup: function(m) { return m; }
	});
})");?>

<?php 
$address_count = $i;
?>

<div style="width: 12.8%; float: left;">
	<h3 class="form-section">Address</h3>
</div>
<div id="<?php echo $model_name;?>_tabs" style="width: 87.2%; float: right;margin-top: -3px;">
	<ul class="nav nav-tabs">
		<?php if($address_count > 0): foreach ($model->addresses as $i => $address):?>
			<?php switch ($i + 1) {
				case '1':
					$title = 'First Address';break;
				case '2':
					$title = 'Second Address';break;
				case '3':
					$title = 'Third Address';break;
				default:
					$title = $i + 1 . 'th Address';
			}?>
			<li><a data-toggle="tab" href="#tab_address_<?php echo $model_name . '_' . $i?>"><i class="icon-reorder"></i> <?php echo $title?></a></li>
		<?php endforeach; else:?>
			<li><a data-toggle="tab" href="#tab_address_<?php echo $model_name; ?>_0"><i class="icon-reorder"></i> First Address</a></li>
		<?php endif;?>

		<li><a data-toggle="tab" href="#tab_new_address_<?php echo $model_name; ?>"><i class="icon-plus"></i>New Address</a></li>
	</ul>
</div>
<div class="clearfix"></div>

<div class="tab-content">
	<?php if($address_count > 0):?>
		<?php foreach ($model->addresses as $i => $address):?>
			<div id="tab_address_<?php echo $model_name . '_' . $i?>" class="tab-pane">
				<?php $this->renderPartial('../address/_partial_form', array('form' => $form, 'model' => $model, 'i' => $i, 'prefix' => '[' .$model_name .']')); ?>
			</div>
		<?php endforeach;?>
	<?php else:?>
		<div id="tab_address_<?php echo $model_name . '_0'?>" class="tab-pane">
			<?php $this->renderPartial('../address/_partial_form', array('form' => $form, 'model' => $model, 'i' => 0, 'prefix' => '[' .$model_name .']')); ?>
		</div>
	<?php endif;?>
	<div id="tab_new_address_<?php echo $model_name;?>" class="tab-pane">
		<?php $this->renderPartial('../address/_partial_form2', array('form' => $form, 'model' => $model, 'prefix' => '[' .$model_name .']')); ?>
	</div>
</div>