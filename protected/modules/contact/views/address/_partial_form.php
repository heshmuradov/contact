<?php

if(empty($prefix)){
  $prefix = '';
}

$i = isset($i) ? $i : 0;

$address_model = (!empty($model->addresses[$i])) ? $model->addresses[$i] : new Address;
?>

	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<div class="input-icon">
						<i class="icon-font"></i>
						<?php echo $form->textField($address_model,$prefix . "[$i]" .'street',array('class'=>'form-control', 'placeholder'=>'Title Address')); ?>
						<?php echo $form->error($address_model,$prefix . "[$i]" .'street'); ?>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<?php echo $form->textArea($address_model,$prefix . "[$i]" .'street',array('class'=>'form-control', 'placeholder'=>'Street, Building, Floor...')); ?>
					<?php echo $form->error($address_model,$prefix . "[$i]" .'street'); ?>
				</div>
			</div>
		</div>
		<!--/span-->
		<div class="col-md-6">
			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<?php echo $form->textField($address_model, $prefix . "[$i]" .'zip', array('class'=>'form-control select2-zip', 'data-placeholder'=>'<i class="icon-location-arrow"></i> Zip Code')); ?>
					<?php echo $form->error($address_model,$prefix . "[$i]" .'zip'); ?>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<?php echo $form->textField($address_model, $prefix . "[$i]" .'city', array('class'=>'form-control select2-city', 'data-placeholder'=>'<i class="icon-location-arrow"></i> City/Town')); ?>
					<?php echo $form->error($address_model,$prefix . "[$i]" .'city'); ?>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<?php echo $form->dropDownList($address_model, $prefix . "[$i]" .'country', array(''=>'')+CHtml::listData(Countries::model()->findAll(array('select'=> 'iso_code, concat("  ",name) name', 'order' => 'name')), 'iso_code', 'name'), array('class'=>'form-control select2-country', 'data-placeholder'=>'<i class="icon-map-marker"></i> Select a Country')); ?>
					<?php echo $form->error($address_model, $prefix . "[$i]" .'country'); ?>
				</div>
			</div>
		</div>
		<!--/span-->
	</div>
	<!--/row-->