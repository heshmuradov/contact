<?php
/* @var $this CompanyController */
/* @var $model Company */
/* @var $form CActiveForm */
?>

<div class="portlet">
	<div class="portlet-title">
		<div class="caption"><i class="icon-reorder"></i><?php echo $model->isNewRecord ? 'Create Company' : 'Update Company ' . $model->id;?></div>
		<!--div class="tools">
			<a class="collapse" href="javascript:;"></a>
			<a class="config" data-toggle="modal" href="#portlet-config"></a>
			<a class="reload" href="javascript:;"></a>
			<a class="remove" href="javascript:;"></a>
		</div-->
	</div>
	<div class="portlet-body form">
		<!-- BEGIN FORM-->
		<?php $form=$this->beginWidget('CActiveForm', array(
			'id'=>'company-form',
			'enableAjaxValidation'=>false,
			'htmlOptions' => array('class' => 'form-horizontal', 'enctype' => 'multipart/form-data'),
          	'enableClientValidation'=>true,
          	'clientOptions'=>array(
            	  'validateOnSubmit'=>true,
            	  'inputContainer' =>'div.form-group',
            	  'errorCssClass'=>'has-error'
          	)
		)); ?>
		
			<div class="form-body">
				<?php $this->renderPartial('_partial_form', array('model' => $model, 'form' => $form)); ?>
			</div>
			
			<div class="form-actions fluid">
				<div class="row">
					<div class="col-md-6">
						<div class="col-md-offset-3 col-md-9">
							<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-success')); ?>
						</div>
					</div>
					<div class="col-md-6">
					</div>
				</div>
			</div>
		<?php $this->endWidget(); ?>
		<!-- END FORM-->                
	</div>
</div>