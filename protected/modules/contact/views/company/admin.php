<?php
/* @var $this CompanyController */
/* @var $model Company */

$this->breadcrumbs=array(
	'Companies'=>array('index'),
	'Manage',
);

Yii::app()->clientScript->registerCss('companyLogo', "
.companyLogo{
 	display: table-cell;
    height: 80px;
    text-align: center;
    vertical-align: middle;
    width: 80px;
	border: 2px solid #DDDDDD;
    box-shadow: 0 1px 3px rgba(0, 0, 0, 0.055);
    line-height: 20px;
    padding: 4px;
    transition: all 0.2s ease-in-out 0s;
}
.companyLogo i{
	font-size: 50px;
    line-height: normal;
}
");
?>

<?php $this->renderPartial('_admin_header'); ?>

<div class="row">
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'company-grid',
	'dataProvider'=>isset($operator) ? $model->owned()->search($operator) : $model->owned()->search(),
	'itemsCssClass'=>'table table-striped table-hover', 
	'template' => '<div class="portlet">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-bookmark"></i>Company List
							</div>
							<div class="tools">
								{summary}
							</div>
						</div>
						<div class="portlet-body">
								{items}
								{pager}
						</div>
						</div>
				   </div>',
	'pagerCssClass'=>'dataTables_paginate paging_bootstrap',
	'pager'=>array(
		'class'=>'CLinkPager',
		'header'=>false,
		//'firstPageCssClass'=>'hidden',
		//'lastPageCssClass'=>'hidden',
		'htmlOptions'=>array('class'=>'pager'),
		'prevPageLabel'=>'← Previous',
		'previousPageCssClass'=>'',
		'nextPageLabel'=>'Next →',
		'nextPageCssClass'=>'',
		'selectedPageCssClass'=>'active',		
		'hiddenPageCssClass'=>'disabled',
	),
	'columns'=>array(
		array('name'=>'id', 'header'=>'#'),
		array('name'=>'logo',
			  'type'=>'html', 
			  'value'=>'!empty($data->logo) ? CHtml::image(Yii::app()->request->baseUrl . str_replace(".", "-80x80.", $data->logo),"", array("width"=>"80px", "height"=>"80px")):
						\'<div class="companyLogo"><i class="icon-upload-alt"></i></div>\' ;'
		),
		array('name'=>'association.name', 'header'=>'Type'),
		'name',
		'phone',
		'fax',
		'email',
		'website',
		array(
			'class'=>'CButtonColumn',
			'template'=>'{view}',
			'viewButtonOptions' => array('class'=>'btn btn-default'),
			'viewButtonImageUrl'=>false,
			'viewButtonLabel'=>'View <i class="icon-eye-open"></i>',
		),
	),
)); ?>
</div>