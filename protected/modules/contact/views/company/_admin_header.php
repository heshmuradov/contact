<?php 
$model_name = ucwords( Yii::app()->controller->id );

Yii::app()->clientScript->registerScript('search-form', "
	$('#search-Company-name').change(function(){
		$('#search-Company-email').val( $(this).val() );
		$('#search-Company-website').val( $(this).val() );
	});
		
	$('#search-Person-first_name').change(function(){
		$('#search-Person-last_name').val( $(this).val() );
		$('#search-Company-company_name').val( $(this).val() );
	});
	$('.search-form').submit(function(){
		$('#person-grid, #company-grid').yiiGridView('update', {
			data: $(this).serialize()
		});
		return false;
	});			
");


?>
<div class="row stats-overview-cont">
	<?php echo CHtml::beginForm(Yii::app()->createUrl("contact/$model_name/index"), 'get', array('class'=>'search-form'));?>
	<?php if( $model_name == 'Company' ):?>
		<?php echo '<div class="input-group col-md-4"><span class="input-group-addon"><i class="icon-search"></i></span>';?>
		<?php echo CHtml::textField("{$model_name}[name]", '', array('id'=>"search-{$model_name}-name", 'class'=>'form-control' )); ?>	
		<?php echo '</div>';?>
		<?php echo CHtml::hiddenField("{$model_name}[email]", '', array('id'=>"search-{$model_name}-email" )); ?>
		<?php echo CHtml::hiddenField("{$model_name}[website]", '', array('id'=>"search-{$model_name}-website" )); ?>
	<?php elseif( $model_name == 'Person' ):?>
		<?php echo '<div class="input-group col-md-4"><span class="input-group-addon"><i class="icon-search"></i></span>';?>
		<?php echo CHtml::textField("{$model_name}[first_name]", '', array('id'=>"search-{$model_name}-first_name", 'class'=>'form-control' )); ?>	
		<?php echo '</div>';?>
		<?php echo CHtml::hiddenField("{$model_name}[last_name]", '', array('id'=>"search-{$model_name}-last_name" )); ?>
		<?php echo CHtml::hiddenField("Company[company_name]", '', array('id'=>"search-Company-company_name" )); ?>		
	<?php endif;?>
	<?php echo CHtml::hiddenField("search-form"); ?>
	<?php echo CHtml::endForm();?>
	
	<div class="col-md-6 col-md-offset-2" style="text-align: right;">
		<a class="icon-btn" href="<?php echo Yii::app()->createUrl( 'contact/company/index' );?>">
			<i class="icon-list-alt"></i>
			<div>Company list</div>
			<span class="badge badge-info"> <?php echo Company::model()->owned()->count();?>
			</span>
		</a>
		<a class="icon-btn" href="<?php echo Yii::app()->createUrl( 'contact/company/create' );?>">
			<i class="icon-plus-sign"></i>
			<div>Add Company</div>
		</a>
		<a class="icon-btn" href="#">
			<i class="icon-download-alt"></i>
			<div>Import</div>
		</a>
		<a class="icon-btn" href="<?php echo Yii::app()->createUrl( 'contact/person/create' );?>">
			<i class="icon-plus-sign"></i>
			<div>Add Person</div>
		</a>
		<a class="icon-btn" href="<?php echo Yii::app()->createUrl( 'contact/person/index' );?>">
			<i class="icon-list-alt"></i>
			<div>Person list</div>
			<span class="badge badge-info"> <?php echo Person::model()->count();?>
			</span>
		</a>
	</div>
</div>
<?php 
if( Yii::app()->controller->action->id == 'admin' ){
	Yii::app()->clientScript->registerCss('pager', "
	.pager > .active > a,
	.pager > .active > a:hover,
	.pager > .active > a:focus {
	    background-color: #428BCA;
	    border-color: #428BCA;
	    color: #FFFFFF;
	    cursor: default;
	    z-index: 2;
	}
	.pager .active > a, .pager .active > a:hover {
	    background: none repeat scroll 0 0 #EEEEEE;
	    border-color: #DDDDDD;
	    color: #333333;
	}
	");
}
?>