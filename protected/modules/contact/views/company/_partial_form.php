<?php
/* @var $this CompanyController */
/* @var $model Company */
/* @var $form CActiveForm */
?>

	<h3 class="form-section">Company Info</h3>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<?php echo $form->dropDownList($model,'association_id', array('' => '')+CHtml::listData(CompanyAssociation::model()->findAll(), 'id', 'name'), array('class'=>'form-control select2', 'data-placeholder'=>'<i class="icon-font"></i> Company Type')); ?>
					<?php echo $form->error($model,'association_id'); ?>
				</div>
                <span class="field_hint" style="display: none;">Some hint</span>
			</div>
		</div>
		<!--/span-->
		<div class="col-md-6">
			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<div class="input-icon">
						<i class="icon-font"></i>
						<?php echo $form->textField($model,'name',array('class'=>'form-control', 'placeholder'=>'Company Name')); ?>
						<?php echo $form->error($model,'name'); ?>
					</div>
                    <span class="field_hint" style="display: none;">Some hint</span>
				</div>
			</div>
		</div>
		<!--/span-->
	</div>
	<!--/row-->
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<div class="input-icon">
						<i class="icon-phone"></i>
						<?php echo $form->textField($model,'phone',array('class'=>'form-control', 'placeholder'=>'Phone')); ?>
						<?php echo $form->error($model,'phone'); ?>
					</div>
                    <span class="field_hint" style="display: none;">Some hint</span>
				</div>
			</div>
		</div>
		<!--/span-->
		<div class="col-md-6">
			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<div class="input-icon">
						<i class="icon-phone"></i>
						<?php echo $form->textField($model,'fax',array('class'=>'form-control', 'placeholder'=>'Fax')); ?>
						<?php echo $form->error($model,'fax'); ?>
					</div>
                    <span class="field_hint" style="display: none;">Some hint</span>
				</div>
			</div>
		</div>
		<!--/span-->
	</div>
	<!--/row-->
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<div class="input-icon">
						<i class="icon-envelope"></i>
						<?php echo $form->textField($model,'email',array('class'=>'form-control', 'placeholder'=>'Email')); ?>
						<?php echo $form->error($model,'email'); ?>
					</div>
                    <span class="field_hint" style="display: none;">Some hint</span>
				</div>
			</div>
		</div>
		<!--/span-->
		<div class="col-md-6">
			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<div class="input-icon">
						<i class="icon-globe"></i>
						<?php echo $form->textField($model,'website',array('class'=>'form-control', 'placeholder'=>'Website')); ?>
						<?php echo $form->error($model,'website'); ?>
					</div>
                    <span class="field_hint" style="display: none;">Some hint website</span>
				</div>
			</div>
		</div>
		<!--/span-->
	</div>
	<!--/row-->

	<?php $this->renderPartial('../address/_address_block', array('model' => $model, 'form'  => $form)); ?>

	<h3 class="form-section">Logo</h3>
	<div class="row">
		<div class="col-md-6">
			<div class="col-md-9 col-md-offset-3">
				<?php 
				$img = ( $model->isNewRecord || empty($model->logo) ) ? '': CHtml::image(Yii::app()->request->baseUrl . $model->logo,"logo",array("max-width"=>200));
				$class = ($img == '') ? 'fileupload-new': 'fileupload-exists';
				?>
					<div class="fileupload <?php echo $class?>" data-provides="fileupload">
						<div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
							<img src="<?php echo Yii::app()->theme->baseUrl?>/images/AAAAAA&amp;text=no+image.gif" alt="">
						</div>
						<div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 10px;">
							<?php echo $img;?>
						</div>
						<div>
							<span class="btn btn-default btn-file">
								<span class="fileupload-new"><i class="icon-paper-clip"></i> Select image</span>
								<span class="fileupload-exists"><i class="icon-undo"></i> Change</span>
								<?php echo $form->fileField($model,'logo'); ?>
							</span>							
							<a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="icon-trash"></i> Remove</a>
							<?php echo $form->error($model,'logo'); ?>
						</div>
					</div>
			</div>
		</div>
	</div>
	
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/plugins/select2/select2_conquer.css" />
	<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/plugins/select2/select2.min.js"></script>

	<script>

		$('#Company_association_id').select2({
			 escapeMarkup: function(m) { return m; }
		});
		
        $(".form-control").mouseenter(function (e) {//console.log(e);
        	if (e.relatedTarget && e.relatedTarget.tagName=="I") {
        		return;
        	}
        	
            var pop = $(this);
            var hint_text = pop.parent().next('span').text();
            pop.popover({
                    'placement': 'left',
                    'html': true,
                    'container': 'body',
                    'content': hint_text,
            });
            pop.popover('show');
        }).mouseout(function (e) {
        	if (e.relatedTarget && e.relatedTarget.tagName!="I") {
        		$(this).popover('hide');
        	}
        });
	</script>
