<?php
/* @var $this CompanyController */
/* @var $model Company */

$this->breadcrumbs=array(
	'Companies'=>array('index'),
	'Create',
);
?>

<?php $this->renderPartial('_admin_header'); ?>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>