<?php

class PersonController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
				'accessControl', // perform access control for CRUD operations
				'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
				array('allow',
						'actions'=>array('index','view','create', 'getPersonList'),
						'users'=>array('@'),
				),
				array('allow',
						'actions'=>array('update','delete'),
						'users'=>array('@'),
						'expression' => array($this, 'isOwner'),
				),
				array('allow', // allow admin user to perform 'admin' and 'delete' actions
						'actions'=>array('admin'),
						'users'=>array('admin'),
				),
				array('deny',  // deny all users
						'users'=>array('*'),
				),
		);
	}

	public function isOwner($user, $rule)
	{
		if ($user->isAdmin()) return TRUE;

		$model = $this->loadModel($_GET['id']);
		return in_array($user->id, CHtml::listData($model->users, 'id', 'id'));
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
				'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Person;
		$user = Yii::app()->user;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		if(isset($_POST['Person']))
		{
			$model->attributes=$_POST['Person'];
			$model->photo = CUploadedFile::getInstance($model,'photo');

			$this->processForm($model);	
			$model->isNewRecord = true; //if processForm throw an Exception, Yii will not handle as we expect. see https://github.com/yiisoft/yii/issues/1253 
		}

		$this->render('create',array(
				'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Person']))
		{
			$old_pic = $model->photo;
			$model->attributes=$_POST['Person'];

			//$model->addresses->attributes = $_POST['Address'];

			$this->processForm($model, $old_pic);
		}

		$this->render('update',array(
				'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		/*$dataProvider=new CActiveDataProvider('Person');
		 $this->render('index',array(
		 		'dataProvider'=>$dataProvider,
		 ));*/

		$model=new Person('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Person']))
			$model->attributes=$_GET['Person'];
		if(isset($_GET['search-form']))
			$operator = 'OR';
		else
			$operator = 'AND';
		if(isset($_GET['Company']))
			$company_name = $_GET['Company']['company_name'];
		else
			$company_name = false;
		$this->render('admin',array(
				'model'=>$model,
				'operator'=>$operator,
				'company_name'=>$company_name,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Person('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Person']))
			$model->attributes=$_GET['Person'];

		$this->render('admin',array(
				'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Person the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Person::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Person $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='person-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	protected function processForm($model, $old_pic = FALSE)
	{
		$transaction = Yii::app()->db->beginTransaction();
		try {
			if($model->validate()){
				if($model->save(false)){
					// "Add or create address" part
					$i=0;
					$model->cleanAddresses();
					$addresses = array();
					foreach ( $_POST['Address']['Person'] as $address_data ){			
						if( count($address_data) == 4 ){  //skip the "New Address" tab and there is one field entered
							$empty = true;
							foreach ( $address_data as $k => $v ){
								if(!empty($v)){
									$empty = false;
									break;
								}
							}
							if($empty){
								continue;
							}
							$address = new Address;
							$address->attributes = $address_data;
							$addresses[] = $address;
							if($address->save()){
								$model->addAddress($address->id);
							}else{
								$model->addresses = $addresses;
								throw new Exception();
							}
						}
					}

					// "Add or create company" part
					if($_POST['company_type'] == 'exist'){
						if(isset($_POST['ExistCompany'])){
							$model->cleanCompanies();
							if(isset($_POST['ExistCompany']['id']) && !empty($_POST['ExistCompany']['id'])){
								$model->addCompany($_POST['ExistCompany']['id']);
							}elseif(is_array($_POST['ExistCompany'])){
								foreach ($_POST['ExistCompany'] as $company) {
									if(!empty($company['id'])){
										$model->addCompany($company['id']);
									}else{
										Yii::app()->user->setFlash('company_error', 'Please select one company from the list or create the new one.');
										throw new Exception('Company Error');
									}
								}
							}
							if(empty($model->companies)){
								Yii::app()->user->setFlash('company_error', 'Please select one company from the list or create the new one.');
								throw new Exception();
							}
						}else{
							Yii::app()->user->setFlash('company_error', 'Company cannot be blank.');
							throw new Exception();
						}

					}elseif($_POST['company_type'] == 'new'){
						$company = new Company;
						$company->attributes = $_POST['Company'];
						$company->logo = CUploadedFile::getInstance($company,'logo');
						if($company->validate()){
							if($company->save(false)){
								$company->cleanAddresses();
								foreach ($_POST['Address']['Company'] as $i => $address_data) {
									if( count($address_data) == 4){  //skip the "New Address" tab
										$address = new Address;
										$address->attributes = $address_data;
										$addresses[] = $address;
										if($address->save()){
											$company->addAddress($address->id);
										}
									}
								}
								if( !empty( $company->logo )){
									$path = $company->saveLogo();
									$company->logo = $path;
									$company->save();
								}
								$model->addCompany($company->id);
							}
						}else{
							throw new Exception();
						}
					}

					// "Add or edit monitoring" part
					if(isset($_POST['Monitoring'])){
						$model->cleanMonitoring();
						foreach ($_POST['Monitoring'] as $monitoring_type_id => $monitoring_data) {
							if(!empty($monitoring_data['person_id'])){
								$model->addMonitoringPerson($monitoring_data['person_id'], $monitoring_type_id);
							}
						}
					}

					// "Add or edit personTypes" part
					if(isset($_POST['type'])){
						$model->cleanPersonTypes();
						foreach ($_POST['type'] as $type_id) {
							$model->addPersonType($type_id);
						}
					}
					
					$uploadedFile = CUploadedFile::getInstance($model,'photo');
					$path = $model->savePhoto($uploadedFile, $old_pic);
					$model->photo = $path;
					$model->save();
					
					$transaction->commit();
					$this->redirect(array('view','id'=>$model->id));
				}
			}
			throw new Exception("Error Processing Request", 1);
		} catch (Exception $e) {
			$transaction->rollback();
			Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
		}
	}

	public function actionGetPersonList($term)
	{
		if (isset($_GET['term'])) {
			$criteria = new CDbCriteria;
			$criteria->addSearchCondition('first_name',$_GET['term']);
			$criteria->addSearchCondition('last_name',$_GET['term'], true, 'OR');

			$people = Person::model()->owned()->findAll($criteria);
			$person_list = array();
			if($people){
				foreach ($people as $person) {
					$person_list[] = array(
							'label' => $person->getFullName(),
							'value' => $person->getFullName(),
							'id' => $person->id,
					);
				}
			}
		}
		echo CJSON::encode($person_list);
		Yii::app()->end();
	}
}
