<?php

class CompanyController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',
				'actions'=>array('index','view','create','getZipList','getCityList','getCountryList', 'getCompanyList'),
				'users'=>array('@'),
			),
			array('allow',
			  'actions'=>array('update','delete'),
			  'users'=>array('@'),
			  'expression' => array($this, 'isOwner'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

  public function isOwner($user, $rule)
  {
    if ($user->isAdmin()) return TRUE;

    $model = $this->loadModel($_GET['id']);
    return in_array($user->id, CHtml::listData($model->users, 'id', 'id'));
  }

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Company;
		$user = Yii::app()->user;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Company']))
		{
			$model->attributes=$_POST['Company'];
			$model->logo = CUploadedFile::getInstance($model,'logo');

			$this->processForm($model);
			$model->isNewRecord = true; //if processForm throw an Exception, Yii will not handle as we expect. see https://github.com/yiisoft/yii/issues/1253
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Company']))
		{
			$old_pic = $model->logo;
			$model->attributes=$_POST['Company'];

			$this->processForm($model, $old_pic);
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		/*$dataProvider=new CActiveDataProvider('Company');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));*/
		$model=new Company('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Company']))
			$model->attributes=$_GET['Company'];
		if(isset($_GET['search-form']))
			$operator = 'OR';
		else
			$operator = 'AND';
		$this->render('admin',array(
			'model'=>$model,
			'operator'=>$operator,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Company('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Company']))
			$model->attributes=$_GET['Company'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Company the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Company::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Company $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='company-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	protected function processForm($model, $old_pic= FALSE)
	{
		$transaction = Yii::app()->db->beginTransaction();
		try {
			if($model->validate()){
				if($model->save(false)){
					$model->cleanAddresses();
					foreach ($_POST['Address'][get_class($model)] as $i => $address_data) {
						if( count($address_data) == 4){  //skip the "New Address" tab
							$address = new Address;
							$address->attributes = $address_data;
							$addresses[] = $address;
							if($address->save()){
								$model->addAddress($address->id);
							}else{
								$model->addresses = $addresses;
								throw new Exception();
							}
						}
					}

					$uploadedFile = CUploadedFile::getInstance($model,'logo');
					$path = $model->saveLogo($uploadedFile, $old_pic);
					$model->logo = $path;
					$model->save();

					$transaction->commit();
					$this->redirect(array('view','id'=>$model->id));
				}
			}
			throw new Exception("Error Processing Request", 1);
		} catch (Exception $e) {
			$transaction->rollback();
			Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
		}
	}

	public function actionGetZipList($term)
	{
           $res =array();

            if (isset($_GET['term'])) {
                    $qtxt ="SELECT
                            concat( t.zip,' ',t.name) as label,
                            t.zip  as zip,
                            t.name as cityName,
                    		t2.iso_code as countryCode
                            FROM ses_cities t
                    		LEFT JOIN ses_countries t2
                    		ON t.country_id = t2.id
                            WHERE t.zip LIKE :qterm
                            ORDER BY t.zip, t.name ASC";
                    $command =Yii::app()->db->createCommand($qtxt);
                    $command->bindValue(":qterm", $_GET['term'].'%', PDO::PARAM_STR);
                    $res =$command->queryAll();
            }

            echo CJSON::encode($res);
            Yii::app()->end();
	}

	public function actionGetCityList($term)
	{
           $res =array();

            if (isset($_GET['term'])) {
                    $qtxt ="SELECT
                            concat( t.name,' ',t.zip) as label,
                            t.zip  as zip,
                            t.name as cityName,
                    		t2.iso_code as countryCode
                            FROM ses_cities t
                    		LEFT JOIN ses_countries t2
                    		ON t.country_id = t2.id
                            WHERE t.name LIKE :qterm
                            ORDER BY t.name ASC";
                    $command =Yii::app()->db->createCommand($qtxt);
                    $command->bindValue(":qterm", $_GET['term'].'%', PDO::PARAM_STR);
                    $res =$command->queryAll();
            }

            echo CJSON::encode($res);
            Yii::app()->end();
	}


	public function actionGetCountryList($term)
	{
           $res =array();

            if (isset($_GET['term'])) {
                    $qtxt ="SELECT
                            t.name as label
                            FROM ses_countries t
                            WHERE t.name LIKE :qterm
                            ORDER BY t.name ASC";
                    $command =Yii::app()->db->createCommand($qtxt);
                    $command->bindValue(":qterm", $_GET['term'].'%', PDO::PARAM_STR);
                    $res =$command->queryAll();
            }

            echo CJSON::encode($res);
            Yii::app()->end();
	}

	public function actionGetCompanyList($term)
	{
		if (isset($_GET['term'])) {
			$criteria = new CDbCriteria;
			$criteria->addSearchCondition('name',$_GET['term']);

			$companies = Company::model()->owned()->findAll($criteria);
			$company_list = array();
			if($companies){
				foreach ($companies as $company) {
					$company_list[] = array(
						'value' => $company->name,
						'label' => $company->name,
						'id' => $company->id,
					);
				}
				// $company_list = CHtml::listData($companies, 'id','name');
			}
		}
		echo CJSON::encode($company_list);
		Yii::app()->end();
	}
}
