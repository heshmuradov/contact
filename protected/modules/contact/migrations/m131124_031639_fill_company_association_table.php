<?php

class m131124_031639_fill_company_association_table extends CDbMigration
{
	public function safeUp()
	{
		$this->insert('ses_company_association', array('name' => 'SARL'));
		$this->insert('ses_company_association', array('name' => 'SA'));
		$this->insert('ses_company_association', array('name' => 'SAS'));
		$this->insert('ses_company_association', array('name' => 'EURL'));
	}

	public function safeDown()
	{
		$this->delete('ses_company_association', array('in','name', array('SARL','SA','SAS','EURL')));
	}
}