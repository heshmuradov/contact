<?php

class m131206_090833_many_to_many_connection_for_address_and_person_tables extends CDbMigration
{
	public function safeUp()
	{
		$this->createTable('ses_addresses_people', array(
			'person_id' => 'integer NOT NULL',
			'address_id' => 'integer NOT NULL'
		),
		'COLLATE=\'utf8_general_ci\'
		ENGINE=InnoDB');
		$this->addForeignKey('fk_ses_addresses_people_person_id', 'ses_addresses_people', 'person_id', 'ses_person', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_ses_addresses_people_address_id', 'ses_addresses_people', 'address_id', 'ses_address', 'id', 'CASCADE', 'CASCADE');

		$address_people = Yii::app()->db->createCommand()
			->select('id, address_id')
			->from('ses_person')
			->queryAll();
		if(!empty($address_people)){
			foreach ($address_people as $item) {
				Yii::app()->db->createCommand()->insert('ses_addresses_people', array('person_id' => $item['id'], 'address_id' => $item['address_id']));
			}
		}

		$this->dropForeignKey('fk_person_address_id', 'ses_person');
		$this->dropColumn('ses_person', 'address_id');
	}

	public function safeDown()
	{
		$this->addColumn('ses_person', 'address_id', 'integer NOT NULL');
		$address_people = Yii::app()->db->createCommand()
			->select('person_id, address_id')
			->from('ses_addresses_people')
			->queryAll();
		if(!empty($address_people)){
			foreach ($address_people as $item) {
				Yii::app()->db->createCommand()->update(
					'ses_person',
					array('address_id' => $item['address_id']),
					'id = :person_id',
					array(':person_id' => $item['person_id']));
			}
		}

		$this->addForeignKey('fk_person_address_id', 'ses_person', 'address_id', 'ses_address', 'id');

		$this->dropForeignKey('fk_ses_addresses_people_person_id', 'ses_addresses_people');
		$this->dropForeignKey('fk_ses_addresses_people_address_id', 'ses_addresses_people');

		$this->dropTable('ses_addresses_people');
	}
}