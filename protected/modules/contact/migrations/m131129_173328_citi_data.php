<?php

class m131129_173328_citi_data extends CDbMigration
{
	public function up()
	{
		Yii::app()->db->createCommand('
			INSERT INTO `ses_cities` (`zip`, `name`) VALUES 
			("33230","Abzac AQUITAINE"),
			("33124","Aillas AQUITAINE"),
			("33440","Ambarès-et-Lagrave AQUITAINE"),
			("33810","Ambès AQUITAINE"),
			("33510","Andernos-les-Bains AQUITAINE"),
			("33390","Anglade AQUITAINE"),
			("33640","Arbanats AQUITAINE"),
			("33760","Arbis AQUITAINE"),
			("33120","Arcachon AQUITAINE"),
			("33460","Arcins AQUITAINE"),
			("33740","Arès AQUITAINE"),
			("33460","Arsac AQUITAINE"),
			("33370","Artigues-près-Bordeaux AQUITAINE"),
			("33500","Arveyres AQUITAINE"),
			("33240","Asques AQUITAINE"),
			("33430","Aubiac AQUITAINE"),
			("33240","Aubie-et-Espessas AQUITAINE"),
			("33980","Audenge AQUITAINE"),
			("33790","Auriolles AQUITAINE"),
			("33124","Auros AQUITAINE"),
			("33480","Avensan AQUITAINE"),
			("33640","Ayguemorte-les-Graves AQUITAINE"),
			("33190","Bagas AQUITAINE"),
			("33760","Baigneaux AQUITAINE"),
			("33730","Balizac AQUITAINE"),
			("33190","Barie AQUITAINE"),
			("33750","Baron AQUITAINE"),
			("33720","Barsac AQUITAINE"),
			("33190","Bassanne AQUITAINE"),
			("33530","Bassens AQUITAINE"),
			("33880","Baurech AQUITAINE"),
			("33230","Bayas AQUITAINE"),
			("33710","Bayon-sur-Gironde AQUITAINE"),
			("33430","Bazas AQUITAINE"),
			("33640","Beautiran AQUITAINE"),
			("33340","Bégadan AQUITAINE"),
			("33130","Bègles AQUITAINE"),
			("33410","Béguey AQUITAINE"),
			("33830","Belin-Béliet AQUITAINE"),
			("33760","Bellebat AQUITAINE"),
			("33760","Bellefond AQUITAINE"),
			("33350","Belvès-de-Castillon AQUITAINE"),
			("33430","Bernos-Beaulac AQUITAINE"),
			("33390","Berson AQUITAINE"),
			("33124","Berthez AQUITAINE"),
			("33750","Beychac-et-Caillau AQUITAINE"),
			("33210","Bieujac AQUITAINE"),
			("33380","Biganos AQUITAINE"),
			("33430","Birac AQUITAINE"),
			("33190","Blaignac AQUITAINE"),
			("33340","Blaignan AQUITAINE"),
			("33290","Blanquefort AQUITAINE"),
			("33540","Blasimon AQUITAINE"),
			("33390","Blaye AQUITAINE"),
			("33670","Blésignac AQUITAINE"),
			("33210","Bommes AQUITAINE"),
			("33370","Bonnetan AQUITAINE"),
			("33910","Bonzac AQUITAINE"),
			("33000","Bordeaux AQUITAINE"),
			("33100","Bordeaux AQUITAINE"),
			("33200","Bordeaux AQUITAINE"),
			("33300","Bordeaux AQUITAINE"),
			("33800","Bordeaux AQUITAINE"),
			("33350","Bossugan AQUITAINE"),
			("33270","Bouliac AQUITAINE"),
			("33190","Bourdelles AQUITAINE"),
			("33710","Bourg AQUITAINE"),
			("33113","Bourideys AQUITAINE"),
			("33480","Brach AQUITAINE"),
			("33420","Branne AQUITAINE"),
			("33124","Brannens AQUITAINE"),
			("33820","Braud-et-Saint-Louis AQUITAINE"),
			("33124","Brouqueyran AQUITAINE"),
			("33520","Bruges AQUITAINE"),
			("33720","Budos AQUITAINE"),
			("33650","Cabanac-et-Villagrains AQUITAINE"),
			("33420","Cabara AQUITAINE"),
			("33750","Cadarsac AQUITAINE"),
			("33140","Cadaujac AQUITAINE"),
			("33410","Cadillac AQUITAINE"),
			("33240","Cadillac-en-Fronsadais AQUITAINE"),
			("33750","Camarsac AQUITAINE"),
			("33880","Cambes AQUITAINE"),
			("33360","Camblanes-et-Meynac AQUITAINE"),
			("33420","Camiac-et-Saint-Denis AQUITAINE"),
			("33190","Camiran AQUITAINE"),
			("33660","Camps-sur-lIsle AQUITAINE"),
			("33390","Campugnan AQUITAINE"),
			("33610","Canéjan AQUITAINE"),
			("33460","Cantenac AQUITAINE"),
			("33760","Cantois AQUITAINE"),
			("33550","Capian AQUITAINE"),
			("33220","Caplong AQUITAINE"),
			("33840","Captieux AQUITAINE"),
			("33560","Carbon-Blanc AQUITAINE"),
			("33121","Carcans AQUITAINE"),
			("33410","Cardan AQUITAINE"),
			("33360","Carignan-de-Bordeaux AQUITAINE")')->execute();
	}

	public function down()
	{
		Yii::app()->db->createCommand('truncate ses_cities')->execute();
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}