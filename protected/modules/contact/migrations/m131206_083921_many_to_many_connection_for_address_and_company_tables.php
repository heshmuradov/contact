<?php

class m131206_083921_many_to_many_connection_for_address_and_company_tables extends CDbMigration
{
	public function safeUp()
	{
		$this->createTable('ses_addresses_companies', array(
			'company_id' => 'integer NOT NULL',
			'address_id' => 'integer NOT NULL'
		),
		'COLLATE=\'utf8_general_ci\'
		ENGINE=InnoDB');
		$this->addForeignKey('fk_ses_addresses_companies_company_id', 'ses_addresses_companies', 'company_id', 'ses_company', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_ses_addresses_companies_address_id', 'ses_addresses_companies', 'address_id', 'ses_address', 'id', 'CASCADE', 'CASCADE');

		$address_comppanies = Yii::app()->db->createCommand()
			->select('id, address_id')
			->from('ses_company')
			->queryAll();
		if(!empty($address_comppanies)){
			foreach ($address_comppanies as $item) {
				Yii::app()->db->createCommand()->insert('ses_addresses_companies', array('company_id' => $item['id'], 'address_id' => $item['address_id']));
			}
		}

		$this->dropForeignKey('fk_company_address_id', 'ses_company');
		$this->dropColumn('ses_company', 'address_id');
	}

	public function safeDown()
	{
		$this->addColumn('ses_company', 'address_id', 'integer NOT NULL');
		$address_comppanies = Yii::app()->db->createCommand()
			->select('company_id, address_id')
			->from('ses_addresses_companies')
			->queryAll();
		if(!empty($address_comppanies)){
			foreach ($address_comppanies as $item) {
				Yii::app()->db->createCommand()->update(
					'ses_company',
					array('address_id' => $item['address_id']),
					'id = :company_id',
					array(':company_id' => $item['company_id']));
			}
		}

		$this->addForeignKey('fk_company_address_id', 'ses_company', 'address_id', 'ses_address', 'id');

		$this->dropForeignKey('fk_ses_addresses_companies_company_id', 'ses_addresses_companies');
		$this->dropForeignKey('fk_ses_addresses_companies_address_id', 'ses_addresses_companies');

		$this->dropTable('ses_addresses_companies');
	}
}