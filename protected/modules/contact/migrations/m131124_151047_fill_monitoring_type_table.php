<?php

class m131124_151047_fill_monitoring_type_table extends CDbMigration
{
	public function safeUp()
	{
		$this->insert('ses_monitoring_type', array('name' => 'Sales representative'));
		$this->insert('ses_monitoring_type', array('name' => 'Support contact'));
	}

	public function safeDown()
	{
		$this->delete('ses_monitoring_type', array('in','name', array('Sales representative','Support contact')));
	}
}