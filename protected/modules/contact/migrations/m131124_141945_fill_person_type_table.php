<?php

class m131124_141945_fill_person_type_table extends CDbMigration
{
	public function safeUp()
	{
		$this->insert('ses_person_type', array('name' => 'Customer'));
		$this->insert('ses_person_type', array('name' => 'Provider'));
		$this->insert('ses_person_type', array('name' => 'Partner'));
		$this->insert('ses_person_type', array('name' => 'Prospect'));
	}

	public function safeDown()
	{
		$this->delete('ses_person_type', array('in','name', array('Customer','Provider','Partner','Prospect')));
	}
}