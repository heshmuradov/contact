<?php

class m131123_093222_connection_between_user_company_and_person extends CDbMigration
{
	public function safeUp()
	{
		$this->createTable('ses_users_companies', array(
			'user_id' => 'int(10) UNSIGNED NOT NULL',
			'company_id' => 'integer NOT NULL'
		),
		'COLLATE=\'utf8_general_ci\'
		ENGINE=InnoDB');
		$this->addForeignKey('fk_users_companies_user_id', 'ses_users_companies', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_users_companies_company_id', 'ses_users_companies', 'company_id', 'ses_company', 'id', 'CASCADE', 'CASCADE');

		$this->createTable('ses_users_people', array(
			'user_id' => 'int(10) UNSIGNED NOT NULL',
			'person_id' => 'integer NOT NULL'
		),
		'COLLATE=\'utf8_general_ci\'
		ENGINE=InnoDB');
		$this->addForeignKey('fk_users_people_user_id', 'ses_users_people', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_users_people_person_id', 'ses_users_people', 'person_id', 'ses_person', 'id', 'CASCADE', 'CASCADE');
	}

	public function safeDown()
	{
		$this->dropTable('ses_users_companies');
		$this->dropTable('ses_users_people');
	}
}