<?php

class m131122_163519_general extends CDbMigration
{
	public function safeUp()
	{
		// Address (use for "person" and "company" tables)
		$this->createTable('ses_address', array(
			'id' => 'pk',
			'street' => 'string NOT NULL',
			'zip' => 'varchar(50) NOT NULL',
			'city' => 'varchar(50) NOT NULL',
			'country' => 'varchar(50) NOT NULL',
		),
		'COLLATE=\'utf8_general_ci\'
		ENGINE=InnoDB');

		// Company
		$this->createTable('ses_company_association', array(
			'id' => 'pk',
			'name' => 'string NOT NULL'
		),
		'COLLATE=\'utf8_general_ci\'
		ENGINE=InnoDB');

		$this->createTable('ses_company', array(
			'id' => 'pk',
			'name' => 'string NOT NULL',
			'phone' => 'varchar(25) NOT NULL',
			'fax' => 'varchar(25) NOT NULL',
			'email' => 'string NOT NULL',
			'website' => 'string NOT NULL',
			'address_id' => 'integer NOT NULL',
			'logo' => 'string',
			'association_id' => 'integer NOT NULL'
		),
		'COLLATE=\'utf8_general_ci\'
		ENGINE=InnoDB');

		$this->addForeignKey('fk_company_address_id', 'ses_company', 'address_id', 'ses_address', 'id');
		$this->addForeignKey('fk_company_association_id', 'ses_company', 'association_id', 'ses_company_association', 'id');

		// Person
		$this->createTable('ses_monitoring_type', array(
			'id' => 'pk',
			'name' => 'string NOT NULL',
		),
		'COLLATE=\'utf8_general_ci\'
		ENGINE=InnoDB');

		$this->createTable('ses_person_type', array(
			'id' => 'pk',
			'name' => 'string NOT NULL'
		),
		'COLLATE=\'utf8_general_ci\'
		ENGINE=InnoDB');

		$this->createTable('ses_person', array(
			'id' => 'pk',
			'office' => 'string',
			'phone' => 'varchar(25) NOT NULL',
			'fax' => 'varchar(25) NOT NULL',
			'email' => 'string NOT NULL',
			'address_id' => 'integer NOT NULL',
			'photo' => 'string'
		),
		'COLLATE=\'utf8_general_ci\'
		ENGINE=InnoDB');
		$this->addForeignKey('fk_person_address_id', 'ses_person', 'address_id', 'ses_address', 'id');

		$this->createTable('ses_people_types', array(
			'person_id' => 'integer NOT NULL',
			'person_type_id' => 'integer NOT NULL'
		),
		'COLLATE=\'utf8_general_ci\'
		ENGINE=InnoDB');
		$this->addForeignKey('fk_people_types_person_id', 'ses_people_types', 'person_id', 'ses_person', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_people_types_person_type_id', 'ses_people_types', 'person_type_id', 'ses_person_type', 'id', 'CASCADE', 'CASCADE');

		$this->createTable('ses_monitoring', array(
			'person_id' => 'integer NOT NULL',
			'monitoring_person_id' => 'integer NOT NULL',
			'monitoring_type_id' => 'integer NOT NULL'
		),
		'COLLATE=\'utf8_general_ci\'
		ENGINE=InnoDB');
		$this->addForeignKey('fk_monitoring_person_id', 'ses_monitoring', 'person_id', 'ses_person', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_monitoring_mon_person_id', 'ses_monitoring', 'monitoring_person_id', 'ses_person', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_monitoring_mon_type_id', 'ses_monitoring', 'monitoring_type_id', 'ses_monitoring_type', 'id');

		$this->createTable('ses_companies_people', array(
			'company_id' => 'integer NOT NULL',
			'person_id' => 'integer NOT NULL'
		),
		'COLLATE=\'utf8_general_ci\'
		ENGINE=InnoDB');
		$this->addForeignKey('fk_companies_people_company_id', 'ses_companies_people', 'company_id', 'ses_company', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_companies_people_person_id', 'ses_companies_people', 'person_id', 'ses_person', 'id', 'CASCADE', 'CASCADE');

	}

	public function safeDown()
	{
		$this->dropTable('ses_companies_people');
		$this->dropTable('ses_monitoring');
		$this->dropTable('ses_people_types');
		$this->dropTable('ses_person');
		$this->dropTable('ses_person_type');
		$this->dropTable('ses_monitoring_type');
		$this->dropTable('ses_company');
		$this->dropTable('ses_company_association');
		$this->dropTable('ses_address');
	}
}