<?php

class m131206_081440_change_required_fields_for_company_table extends CDbMigration
{
	public function safeUp()
	{
		$this->alterColumn('ses_company', 'phone', 'varchar(25)');
		$this->alterColumn('ses_company', 'fax', 'varchar(25)');
		$this->alterColumn('ses_company', 'email', 'string');
		$this->alterColumn('ses_company', 'website', 'string');
	}

	public function safeDown()
	{
		$this->alterColumn('ses_company', 'phone', 'varchar(25) NOT NULL');
		$this->alterColumn('ses_company', 'fax', 'varchar(25) NOT NULL');
		$this->alterColumn('ses_company', 'email', 'string NOT NULL');
		$this->alterColumn('ses_company', 'website', 'string NOT NULL');
	}
}