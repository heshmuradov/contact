<?php

class m131129_140015_alter_city_country extends CDbMigration
{
	public function up()
	{
		Yii::app()->db->createCommand('ALTER TABLE `ses_cities` ADD `zip` VARCHAR( 50 ) NOT NULL ')->execute();

// ability to enter city/country which are not exist in db
		Yii::app()->db->createCommand('
			ALTER TABLE `ses_address` CHANGE `city` `city` VARCHAR( 100 ) NOT NULL ,
			CHANGE `country` `country` VARCHAR( 100 ) NOT NULL ')->execute();

	}


	public function down()
	{ 
		Yii::app()->db->createCommand('ALTER TABLE `ses_cities` DROP `zip`');
		Yii::app()->db->createCommand('
			ALTER TABLE `ses_address` CHANGE `city` `city` int( 11 ) NOT NULL ,
			CHANGE `country` `country` int( 11 ) NOT NULL ')->execute();
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}