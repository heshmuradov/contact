<?php

class m131206_082236_change_required_fields_for_person_table extends CDbMigration
{
	public function safeUp()
	{
		$this->alterColumn('ses_person', 'phone', 'varchar(25)');
		$this->alterColumn('ses_person', 'fax', 'varchar(25)');
		$this->alterColumn('ses_person', 'gender', 'varchar(1)');
		$this->alterColumn('ses_person', 'date_of_birth', 'date');
		$this->alterColumn('ses_person', 'mobile_phone', 'string');
	}

	public function safeDown()
	{
		$this->alterColumn('ses_person', 'phone', 'varchar(25) NOT NULL');
		$this->alterColumn('ses_person', 'fax', 'varchar(25) NOT NULL');
		$this->alterColumn('ses_person', 'gender', 'varchar(1) NOT NULL');
		$this->alterColumn('ses_person', 'date_of_birth', 'date NOT NULL');
		$this->alterColumn('ses_person', 'mobile_phone', 'string NOT NULL');
	}
}