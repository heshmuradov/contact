<?php

class m131201_060349_add_new_fields_for_person_table extends CDbMigration
{
	public function safeUp()
	{
		$this->addColumn('ses_person', 'first_name', 'string NOT NULL');
		$this->addColumn('ses_person', 'last_name', 'string NOT NULL');
		$this->addColumn('ses_person', 'gender', 'varchar(1) NOT NULL');
		$this->addColumn('ses_person', 'date_of_birth', 'date NOT NULL');
		$this->addColumn('ses_person', 'mobile_phone', 'string NOT NULL');
	}

	public function safeDown()
	{
		$this->dropColumn('ses_person', 'first_name');
		$this->dropColumn('ses_person', 'last_name');
		$this->dropColumn('ses_person', 'gender');
		$this->dropColumn('ses_person', 'date_of_birth');
		$this->dropColumn('ses_person', 'mobile_phone');
	}
}