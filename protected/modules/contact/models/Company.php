<?php

/**
 * This is the model class for table "ses_company".
 *
 * The followings are the available columns in table 'ses_company':
 * @property integer $id
 * @property string $name
 * @property string $phone
 * @property string $fax
 * @property string $email
 * @property string $website
 * @property string $logo
 * @property integer $association_id
 *
 * The followings are the available model relations:
 * @property Person[] $people
 * @property Address[] $addresses
 * @property CompanyAssociation $association
 * @property YumUser[] $users
 */
class Company extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Company the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ses_company';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, association_id', 'required'),
			array('association_id', 'numerical', 'integerOnly'=>true),
			array('name, email, website, logo', 'length', 'max'=>255),
			array('logo', 'EImageValidator', 'types' => 'gif, jpg, png', 
					'allowEmpty'=>true, 
					'maxSize'=>100 * 1024, //100 Kb
					'tooLarge'=>Yii::t('yii','The file "{file}" is too large. Its size cannot exceed 100 Kb.'),
					'width'=>80,
					'height'=>80, 
					//'on'=>'update'
					),
			array('phone, fax', 'length', 'max'=>25),
			array('website', 'url'),
			array('email', 'email'),
			array('phone', 'match', 'pattern'=>'/^([+]?[0-9 ]+)$/'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, phone, fax, email, website, logo, association_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'people' => array(self::MANY_MANY, 'Person', 'ses_companies_people(company_id, person_id)'),
			'addresses' => array(self::MANY_MANY, 'Address', 'ses_addresses_companies(company_id, address_id)'),
			'association' => array(self::BELONGS_TO, 'CompanyAssociation', 'association_id'),
			'users' => array(self::MANY_MANY, 'YumUser', 'ses_users_companies(company_id, user_id)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Company name',
			'phone' => 'Phone',
			'fax' => 'Fax',
			'email' => 'Email',
			'website' => 'Website',
			'addresses' => 'Address',
			'logo' => 'Logo',
			//'association_id' => 'Association',
			'association_id' => 'Company Type',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search( $operator = 'AND')
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true, $operator);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('fax',$this->fax,true);
		$criteria->compare('email',$this->email,true, $operator);
		$criteria->compare('website',$this->website,true, $operator);
		$criteria->compare('logo',$this->logo,true);
		$criteria->compare('association_id',$this->association_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


	public function owned()
	{
		$criteria = $this->getDbCriteria();
		$criteria->with = array(
			'users'=>array(
				'select'=>false,
				'joinType'=>'INNER JOIN',
				// 'condition'=>'',
			)
		);
		$criteria->together = true;

		$criteria->compare('users.id', Yii::app()->user->id);
		return $this;
	}

	public function afterSave()
	{
		$user = Yii::app()->user;
		if($user && $this->scenario == 'insert'){
			Yii::app()->db->createCommand()->insert('ses_users_companies', array('user_id' => $user->id, 'company_id' => $this->id));
		}
	}
	
	protected function afterDelete(){
		parent::afterDelete();
		
		if( file_exists(Yii::getPathOfAlias('webroot').$this->logo) )
			unlink(Yii::getPathOfAlias('webroot').$this->logo);
		if( file_exists(Yii::getPathOfAlias('webroot').str_replace('.', '-80x80.', $this->logo)) )
			unlink(Yii::getPathOfAlias('webroot').str_replace('.', '-80x80.', $this->logo));

	} 

	public function saveLogo($uploadedFile = null, $old_pic = FALSE)
	{
		if($uploadedFile != null){
			$user = Yii::app()->user;
			$path = '/images/uploads/logo/' . $user->id . '/' . uniqid() . '-' . $uploadedFile->getName();
			$folder_path = Yii::getPathOfAlias('webroot') . '/images/uploads/logo/' . $user->id;
			if(!file_exists($folder_path)){
				mkdir($folder_path, 0777, true);
			}
			$uploadedFile->saveAs(Yii::getPathOfAlias('webroot') . $path);
			
			//create a 80x80 size used for index page
			include(Yii::getPathOfAlias('application')."/models/resize-class.php");
			$resizeObj = new resize(Yii::getPathOfAlias('webroot') . $path);
			$resizeObj -> resizeImage(80, 80, 'crop');
			$admin_page_pic = Yii::getPathOfAlias('webroot') . $path;
			$admin_page_pic = str_replace('.', '-80x80.', $admin_page_pic);
			$resizeObj -> saveImage($admin_page_pic, 100);
		}else{
			$path = '';
		}
		
		//delete old pic
		if( $old_pic ){
			if( file_exists(Yii::getPathOfAlias('webroot').$old_pic) )
				unlink(Yii::getPathOfAlias('webroot').$old_pic);
			if( file_exists(Yii::getPathOfAlias('webroot').str_replace('.', '-80x80.', $old_pic)) )
				unlink(Yii::getPathOfAlias('webroot').str_replace('.', '-80x80.', $old_pic));
		}
		return $path;
	}

	public function cleanAddresses()
	{
		Yii::app()->db->createCommand()->delete('ses_addresses_companies', 'company_id = :company_id ', array('company_id' => $this->id));
	}
	public function addAddress($address_id)
	{
		Yii::app()->db->createCommand()->insert('ses_addresses_companies', array('company_id' => $this->id, 'address_id' => $address_id));
	}
}