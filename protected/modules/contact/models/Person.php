<?php

/**
 * This is the model class for table "ses_person".
 *
 * The followings are the available columns in table 'ses_person':
 * @property integer $id
 * @property string $office (removed 2013/12/16)
 * @property string $phone
 * @property string $fax
 * @property string $email
 * @property string $photo
 * @property string $first_name
 * @property string $last_name
 * @property string $gender
 * @property string $date_of_birth
 * @property string $mobile_phone
 *
 * The followings are the available model relations:
 * @property companies[] $companies
 * @property Person[] $monitoringBy
 * @property Person[] $monitoringFor
 * @property PersonType[] $types
 * @property Address[] $addresses
 * @property YumUser[] $users
 */
class Person extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Person the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ses_person';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('phone, fax, email, first_name, last_name, gender, date_of_birth, mobile_phone', 'required'),
			array('office, email, photo, first_name, last_name, mobile_phone', 'length', 'max'=>255),
			array('photo', 'EImageValidator', 'types' => 'gif, jpg, png', 
					'allowEmpty'=>true, 
					'maxSize'=>100 * 1024, //100 Kb
					'tooLarge'=>Yii::t('yii','The file "{file}" is too large. Its size cannot exceed 100 Kb.'),
					'width'=>80,
					'height'=>80),
			array('phone, fax', 'length', 'max'=>25),
			array('email', 'email'),
			array('phone, mobile_phone', 'match', 'pattern'=>'/^([+]?[0-9 ]+)$/'),
			array('gender', 'length', 'max'=>1),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('person_groups, id, office, phone, fax, email, photo, first_name, last_name, gender, date_of_birth, mobile_phone', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'companies' => array(self::MANY_MANY, 'Company', 'ses_companies_people(person_id, company_id)'),
			'monitoringFor' => array(self::MANY_MANY, 'Person', 'ses_monitoring(monitoring_person_id, person_id)'),
			'monitoringBy' => array(self::MANY_MANY, 'Person', 'ses_monitoring(person_id, monitoring_person_id)'),
			'types' => array(self::MANY_MANY, 'PersonType', 'ses_people_types(person_id, person_type_id)'),
			'addresses' => array(self::MANY_MANY, 'Address', 'ses_addresses_people(person_id, address_id)'),
			'users' => array(self::MANY_MANY, 'YumUser', 'ses_users_people(person_id, user_id)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'office' => 'Office',
			'phone' => 'Mobile Phone',
			'fax' => 'Fax',
			'email' => 'Email',
			'addresses' => 'Address',
			'photo' => 'Photo',
			'first_name' => 'First Name',
			'last_name' => 'Last Name',
			'gender' => 'Gender',
			'date_of_birth' => 'Date Of Birth',
			'mobile_phone' => 'Mobile Phone',
		);
	}
public $person_groups;
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search( $operator = 'AND', $withCompany = false )
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('t.id',$this->id);
		$criteria->compare('t.office',$this->office,true);
		$criteria->compare('t.phone',$this->phone,true);
		$criteria->compare('t.fax',$this->fax,true);
		$criteria->compare('t.email',$this->email,true);
		$criteria->compare('t.photo',$this->photo,true);
		$criteria->compare('t.first_name',$this->first_name,true, $operator);
		$criteria->compare('t.last_name',$this->last_name,true, $operator);
		$criteria->compare('t.gender',$this->gender,true);
		$criteria->compare('t.date_of_birth',$this->date_of_birth,true);
		$criteria->compare('t.mobile_phone',$this->mobile_phone,true);

		$criteria->with = array('types', 'companies');
		$criteria->together = true;
		if( !empty($this->person_groups) )
			$criteria->addInCondition('types.id', array($this->person_groups));

		if( $withCompany ){		
			$criteria->compare("companies.name", $withCompany, true, $operator);
		}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>array(
					'attributes'=>array(
							'person_companies'=>array(
									'asc'=>'companies.name',
									'desc'=>'companies.name DESC',
							),
							'*',
					),
			),
		));
	}

	public function owned()
	{
		$criteria = $this->getDbCriteria();
		$criteria->with = array(
			'users'=>array(
				'select'=>false,
				'joinType'=>'INNER JOIN',
				// 'condition'=>'',
			)
		);
		$criteria->together = true;

		$criteria->compare('users.id', Yii::app()->user->id);
		return $this;
	}

	public function afterSave()
	{
		$user = Yii::app()->user;
		if($user && $this->scenario == 'insert'){
			Yii::app()->db->createCommand()->insert('ses_users_people', array('user_id' => $user->id, 'person_id' => $this->id));
		}
	}

	protected function afterDelete(){
		parent::afterDelete();
	
		if( file_exists(Yii::getPathOfAlias('webroot').$this->photo) )
			unlink(Yii::getPathOfAlias('webroot').$this->photo);
		if( file_exists(Yii::getPathOfAlias('webroot').str_replace('.', '-80x80.', $this->photo)) )
			unlink(Yii::getPathOfAlias('webroot').str_replace('.', '-80x80.', $this->photo));
	
	}
	
	public function getFullName()
	{
		return (!empty($this->first_name) || !empty($this->last_name)) ? $this->first_name . ' ' .$this->last_name : '';
	}

	public function savePhoto($uploadedFile = null, $old_pic = false)
	{
		if($uploadedFile != null){
			$user = Yii::app()->user;
			$path = '/images/uploads/photo/' . $user->id . '/' . uniqid() . '-' . $uploadedFile->getName();
			$folder_path = Yii::getPathOfAlias('webroot') . '/images/uploads/photo/' . $user->id;
			if(!file_exists($folder_path)){
				mkdir($folder_path, 0777, true);
			}
			$uploadedFile->saveAs(Yii::getPathOfAlias('webroot') . $path);
			
			//create a 80x80 size used for index page
			include(Yii::getPathOfAlias('application')."/models/resize-class.php");
			$resizeObj = new resize(Yii::getPathOfAlias('webroot') . $path);
			$resizeObj -> resizeImage(80, 80, 'crop');
			$admin_page_pic = Yii::getPathOfAlias('webroot') . $path;
			$admin_page_pic = str_replace('.', '-80x80.', $admin_page_pic);	
			$resizeObj -> saveImage($admin_page_pic, 100);
		}else{
			$path = '';
		}

		//delete old pic
		if( $old_pic ){
			if( file_exists(Yii::getPathOfAlias('webroot').$old_pic) )
				unlink(Yii::getPathOfAlias('webroot').$old_pic);
			if( file_exists(Yii::getPathOfAlias('webroot').str_replace('.', '-80x80.', $old_pic)) )
				unlink(Yii::getPathOfAlias('webroot').str_replace('.', '-80x80.', $old_pic));
		}
		return $path;
	}

	public function getMonitoringPerson($monitoring_type_id)
	{
		$person_data = Yii::app()->db->createCommand()
			->select('p.*')
			->from('ses_monitoring m')
			->leftJoin('ses_person p', 'p.id = m.monitoring_person_id')
			->where('m.person_id = :person_id', array(':person_id' => $this->id))
			->andWhere('m.monitoring_type_id = :monitoring_type_id', array(':monitoring_type_id' => $monitoring_type_id))
			->queryRow();
		$person = new Person;
		$person->attributes = $person_data;
		$person->id = $person_data['id'];
		return $person;
	}

	public function cleanCompanies()
	{
		Yii::app()->db->createCommand()->delete('ses_companies_people', 'person_id = :person_id ', array('person_id' => $this->id));
	}

	public function addCompany($company_id)
	{
		Yii::app()->db->createCommand()->insert('ses_companies_people', array('person_id' => $this->id, 'company_id' => $company_id));
	}

	public function cleanMonitoring()
	{
		Yii::app()->db->createCommand()->delete('ses_monitoring', 'person_id = :person_id ', array('person_id' => $this->id));
	}
	public function addMonitoringPerson($person_id, $monitoring_type_id)
	{
		Yii::app()->db->createCommand()->insert('ses_monitoring', array('person_id' => $this->id, 'monitoring_person_id' => $person_id, 'monitoring_type_id' => $monitoring_type_id));
	}

	public function cleanPersonTypes()
	{
		Yii::app()->db->createCommand()->delete('ses_people_types', 'person_id = :person_id ', array('person_id' => $this->id));
	}
	public function addPersonType($type_id)
	{
		Yii::app()->db->createCommand()->insert('ses_people_types', array('person_id' => $this->id, 'person_type_id' => $type_id));
	}
	
	public function cleanAddresses()
	{
		Yii::app()->db->createCommand()->delete('ses_addresses_people', 'person_id = :person_id ', array('person_id' => $this->id));
	}	
	public function addAddress($address_id)
	{
		Yii::app()->db->createCommand()->insert('ses_addresses_people', array('person_id' => $this->id, 'address_id' => $address_id));
	}
	
	public function getPersonTypes(){
		$html = '';
		$person = Person::model()->findByPk($this->id); //reload person model, make sure the types is not load by CDbCriteria
		foreach ($person->types as $person_type) {
			$html .= '<div class="person_type_list '.$person_type->name.'"></div>';
		}
		return $html;
	}
}