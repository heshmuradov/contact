<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
Yii::setPathOfAlias('uploads', dirname(__FILE__) . '/../../images/uploads');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Agenda',
  'theme' => 'conquer',
  //'defaultController' => 'user/user/login',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
    'application.modules.user.models.*',
    'application.extensions.behaviors.*',
    'application.components.widgets.*',
	),

	'modules'=>array(
    'contact' => array(
    ),
    /*
    'user' => array(
			'debug' => true,
		),*/
    'user' => array(
      'debug' => false,
      'userTable' => 'user',
      'translationTable' => 'translation',
      'passwordRequirements' => array(
        'minLen' => 4,
      ),
      'loginType' => 1,
      'loginLayout' => 'webroot.themes.conquer.views.user.layouts.yum',
      'baseLayout' => 'webroot.themes.conquer.views.layouts.main',
    ),
    'registration' => array(
      'enableRegistration' => false,
      'enableRecovery' => true,
      'layout' => 'webroot.themes.conquer.views.user.layouts.yum',
      'recoverPasswordView' => 'webroot.themes.conquer.views.registration.registration.recovery'
    ),
    /*
    'usergroup' => array(
      'usergroupTable' => 'usergroup',
      'usergroupMessageTable' => 'user_group_message',
    ),*/
    /*'membership' => array(
      'membershipTable' => 'membership',
      'paymentTable' => 'payment',
    ),*/
    /*
    'friendship' => array(
      'friendshipTable' => 'friendship',
    ),*/
    'profile' => array(
      'privacySettingTable' => 'privacysetting',
      'profileFieldTable' => 'profile_field',
      'profileTable' => 'profile',
      'profileCommentTable' => 'profile_comment',
      'profileVisitTable' => 'profile_visit',
      'enableProfileComments' => false,
    ),
    /*'role' => array(
      'roleTable' => 'role',
      'userRoleTable' => 'user_role',
      'actionTable' => 'action',
      'permissionTable' => 'permission',
    ),*/
    /*
    'message' => array(
      'messageTable' => 'message',
    ),*/
	),

	// application components
	'components'=>array(
    'clientScript' => array(
      'packages'=>array(
        'jquery'=>array(
          'basePath' => 'webroot.themes.conquer.assets.plugins',
          'js'=>array('jquery-1.10.2.min.js'),
        ),
        'jquery.migrate' => array(
          'basePath' => 'webroot.themes.conquer.assets.plugins',
          'js'=>array('jquery-migrate-1.2.1.min.js'),
          'depends'=>array('jquery'),
        ),
        'jquery.ui' => array(
          'basePath' => 'webroot.themes.conquer.assets.plugins.jquery-ui',
          'js'=>array('jquery-ui-1.10.3.custom.min.js'),
          'depends'=>array('jquery'),
        )
      ),
    ),
		'user'=>array(
      'class' => 'application.modules.user.components.YumWebUser',
      'allowAutoLogin'=>true,
      // enable cookie-based authentication
      'loginUrl' => array('//user/user/login'),
		),

    'cache' => array('class' => 'system.caching.CDummyCache'),
		// uncomment the following to enable URLs in path-format

		'urlManager'=>array(
			//'urlFormat'=>'path',
			//'rules'=>array(
			//	'<controller:\w+>/<id:\d+>'=>'<controller>/view',
			//	'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
			//	'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			//),
		),

		//'db'=>array(
		//	'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
		//),
		// uncomment the following to use a MySQL database

		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=yujy_contact',
			'emulatePrepare' => true,
			'username' => 'yujy_contact',
      'password' => 'HtLLVptdMHGuMDUw',
			'charset' => 'utf8',
			'schemaCachingDuration' => 3600,
			'autoCommit' => true,
		),

    //'authManager'=>array(
    //  'class'=>'CDbAuthManager',
    //  'connectionID'=>'db',
    //  'defaultRoles'=>array('authenticated', 'admin', 'owner'),
    //),

		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning' . (YII_DEBUG ? ', info' : ''),
				),
				// uncomment the following to show log messages on web pages

				array(
					'class'=>'CWebLogRoute',
					'enabled' => YII_DEBUG,
				),

			),
		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params' => require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'params.php'),
  //'cache'=>array(
  //  'class'=>'CDummyCache',
  //),
);
