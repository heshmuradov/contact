<?php

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Agenda Console Application',

	// preloading 'log' component
	'preload'=>array('log'),

	'import'=>array(
		'application.models.*',
	),

	// application components
	'components'=>array(
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=yujy_contact',
			'emulatePrepare' => true,
			'username' => 'yujy_contact',
			'password' => 'HtLLVptdMHGuMDUw',
			'charset' => 'utf8',
			'schemaCachingDuration' => 3600,
		),
		'log'=>array(
			'enabled' => YII_DEBUG,
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
			),
		),
	),
	'params' => require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'params.php'),
);