<?php

return CMap::mergeArray(
	require(dirname(__FILE__).'/console.php'),
	array(
		'components'=>array(
			'db'=>array(
				'connectionString' => 'mysql:host=localhost;dbname=benoit_agenda',
				'username' => 'root',
				'password' => 'admin',
				'schemaCachingDuration' => 0,
			),
		),
	)
);
