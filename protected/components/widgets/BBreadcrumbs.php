<?php

Yii::import('zii.widgets.CBreadcrumbs');

class BBreadcrumbs extends CBreadcrumbs
{
	public $tagName = 'ul';
  public $htmlOptions = array('class'=>'page-breadcrumb breadcrumb');
	public $separator=' <i class="icon-angle-right"></i> ';
	public $activeLinkTemplate='<li><a href="{url}">{label}</a>{separator}</li>';
	public $inactiveLinkTemplate='<li><a href="{url}">{label}</a></li>';

	public function run()
	{
		if(empty($this->links))
			return;

		echo CHtml::openTag($this->tagName,$this->htmlOptions)."\n";
		$links=array();
		if($this->homeLink===null)
			$links[]='<li><i class="icon-home"></i>' .CHtml::link(Yii::t('zii','Home'),Yii::app()->homeUrl) . $this->separator . '</li>';
		elseif($this->homeLink!==false)
			$links[]=$this->homeLink . $this->separator;

		foreach($this->links as $label=>$url)
		{
			if(!is_string($label) && ! is_array($url)){
				$label = $url;
				$url = Yii::app()->request->getUrl();
				$template = $this->inactiveLinkTemplate;
			}else{
				$template = $this->activeLinkTemplate;
			}

			$links[]=strtr($template,array(
				'{url}'=>CHtml::normalizeUrl($url),
				'{label}'=>$this->encodeLabel ? CHtml::encode($label) : $label,
				'{separator}' => $this->separator,
			));
		}
		echo implode(' ',$links);
		echo CHtml::closeTag($this->tagName);
	}
}