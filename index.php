<?php
/* Set internal character encoding to UTF-8 */
mb_internal_encoding("UTF-8");

error_reporting(E_ALL);
//error_reporting(E_ERROR);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

// change the following paths if necessary
$yii=dirname(__FILE__).'/../framework/yii.php';
$config=dirname(__FILE__).'/protected/config/main.php';
// local
// $config=dirname(__FILE__).'/protected/config/local.php';

// remove the following lines when in production mode
defined('YII_DEBUG') or define('YII_DEBUG',FALSE);
// specify how many levels of call stack should be shown in each log message
// defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);

require_once($yii);
Yii::createWebApplication($config)->run();
